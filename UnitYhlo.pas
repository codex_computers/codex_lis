unit UnitYhlo;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;
    procedure RxChar(Sender: TObject; Count: Integer; comm: tVaComm);
    procedure YhloEndRecord(pfields: tstrings; pcomm:tVacomm);
    procedure YhloPatientRecord(pfields: tstrings);
    procedure YhloRequestQuery(pfields: tstrings);
    procedure YhloResultrecord(pfields: tstrings);
    procedure YhloHeaderRecord(pfields: tstrings);
    procedure YhloAnswerQuery(pstatus,pbarcode:string; pserial:integer; pcomm: tVaComm);
    procedure YhloAssayRecord(pfields: tstrings);
    Procedure Yhlo_parse(data:ansistring; pcomm:tVacomm);
    Procedure YhloCreateRecord(p_fields:Tstrings; p_sep:char; var p_record:string);
    Procedure YhloCreateHeader(var precord:string);
    Procedure YhloCreatePatient(var precord:string);
    Procedure YhloCreateAssay(pbarcode:string; pserial:integer;  pAnalyzerid:string; var precord:string);
    Procedure YhloCreateEnd(var precord:string);
    function Calculatechecksum(msg:string):string;
var
    state:string;    //status na app dali prima rezultati ili prakja aliasi
    state_send:string;  //status za prakjanje na kontrolni karaktri pri odgovor
    barcode:string;
    serialno:integer;
    Fmessage:string;
const
    pAnalyzer:string='Yhlo IFLASH'; //globalna promenliva za naziv na analyzer/protokol
implementation
uses unit3, dm, UnitCommon;

function Calculatechecksum(msg:string):string;
var i, sum, cs:integer;
    hex:string;
begin
     sum:=0;
     for i:=1 to length(msg) do
     begin
          sum:=sum+ord(msg[i]);
     end;
     cs:= sum mod 256;
     hex:=inttohex(cs, 2);
     result:=hex;
end;

procedure RxChar(Sender: TObject; Count: Integer; comm:tVaComm);
var data:string;
i:integer;
begin

     data:=comm.ReadText;
     if FrmMain.mode='debug' then FrmMain.memo2.Lines.add(data);
     for I := 1 to Length(data) do
     case data[I] of
      #2:;  //STX
      #3:;  //ETX
      #5: comm.WriteText(#6);  //ENQ
      #4:begin   //EOT

             if state='query' then  YhloAnswerQuery('',barcode, serialno,comm); //inicijalizacija za response
         end;
      #6:if (state='query') and (state_send<>'') then YhloAnswerQuery(state_send,barcode, serialno,comm);
      #13:; //CR
      #10:  //LF za obrabotka na porakata
        begin
          Yhlo_parse(FMessage, comm);
         // memo2.Lines.Add(fmessage);
          FMessage := '';  //reset received message
          comm.WriteText(#6);    //ACK na LF
        end;
      else
        FMessage := FMessage + data[I];
     end;
end;




Procedure Yhlo_parse(data:ansistring; pcomm:tVacomm);
var fields:tstrings;
i:integer;
begin
  data:=copy(data, 1, length(data)-2); //da mi ostane cist message bez specijalni znakovi,prethodno se eliinirani ETX i STX
  fields:=TStringlist.Create;
  dm1.splitstring(data, '|', fields);

    case  IndexStr (copy(fields[0],2,1), ['H','Q', 'R','P','L','O']) of
    0: YhloHeaderRecord(fields);
    1: YhloRequestQuery(fields);
    2: YhloResultrecord(fields);
    3: YhloPatientRecord(fields);
    4: YhloEndRecord(fields, pcomm);
    5: YhloAssayRecord(fields);
    end;

end;

Procedure YhloHeaderRecord(pfields:tstrings);
begin
    // frmmain.Memo1.Lines.Add('ANALYZER:'+pfields[4]);
end;
Procedure YhloRequestQuery(pfields:tstrings);
begin
     FrmMain.SetTextColor('red');
     frmMain.Memo1.Lines.Add('ANALYZER='+panalyzer+'    Request for sampleID:'+copy(pfields[2],2, length(pfields[2]))+'    Ser.no='+pfields[1]);
     FrmMain.SetTextColor('black');
     serialno:=strtoint(pfields[1]);
     barcode:=copy(pfields[2],2,length(pfields[2]));

     state:='query';   //rezim na baranje analizi za barkod
end;
Procedure YhloResultrecord(pfields:tstrings);
var  pserial,passay, presult, punit, pflag, display_msg, presult_type:string;
     assayname, results:tstrings;
begin

     if state='result' then
     begin
          assayname:=tstringlist.Create;
          results:=tstringlist.Create;
          pserial:=pfields[1];
          dm1.splitstring(pfields[2],'^',assayname);
          passay:=assayname[0];
          presult_type:=assayname[3];  //kvalitativen ili kvantiativen rez
          dm1.SplitString(pfields[3],'^',results);
          if presult_type='F' then
          presult:=results[0] else presult:=results[1];
          punit:=pfields[4];
          pflag:=pfields[7];
          display_msg:='     ANALYZER='+panalyzer+'    SAMPLEID='+barcode+'    TEST='+passay+'   '+'RESULT='+presult+'    FLAG='+pflag+'    UNIT='+punit;
          FrmMain.Memo1.Lines.Add(display_msg);
          assayname.Free;
          results.Free;
         if (presult<>'') then
          begin
               dm1.resultsbuffer.Append;
               dm1.resultsbufferSampleid.Value:=barcode;
               dm1.resultsbufferresult.Value:=trim(presult);
               dm1.resultsbuffertest.value:=passay;
               dm1.resultsbufferunit.value:=punit;
               dm1.resultsbuffer.Post;
          end;
     end;
end;
Procedure YhloPatientRecord(pfields:tstrings);
begin
      serialno:=strtoint(pfields[1]);
      state:='result';
end;
Procedure YhloEndRecord(pfields:tstrings; pcomm:tVacomm);
begin
    // if (pfields[2]='N')  then
     //   pcomm.WriteText(#6);

end;
Procedure YhloAssayRecord(pfields:tstrings);
var  pserial, passay:string;
begin
     if (state='result') and (pfields[25]='F') then
     begin
        pserial:=pfields[1];
        barcode:=pfields[3];
        if strtoint(pserial)=serialno then //proverka dali se rabote za istiot serial t.e. za istata poraka
        begin
           if not (dm1.PatientsBuffer.Locate('sampleID',barcode,[])) then
           begin
                dm1.PatientsBuffer.Append;   //zapis na barkod dokolku ne postoi vo buffer dataset
                dm1.PatientsBuffersampleID.Value:=barcode;
                dm1.PatientsBuffer.Post;
           end;
        end;
     end;
end;
Procedure YhloAnswerQuery(pstatus, pbarcode:string; pserial:integer; pcomm: tVaComm); //podgoovk za prakjanje na aliasi po barkod
var i:integer;
    messagerecord:String;
    display_msg:string;
    p_Analyzerid:string;
begin
     case IndexStr (pstatus, ['','enq', 'H','P','O','L','eot']) of
     0:begin
              pcomm.WriteText(#5);
              state_send:='enq';
      end;
     1:begin
              YhloCreateHeader(messagerecord);
              pcomm.WriteText(messagerecord);
              state_send:='H';
       end;
     2:begin
              YhloCreatePatient(messagerecord);
              pcomm.WriteText(messagerecord);
              state_send:='P';
      end;
      3:begin
              p_Analyzerid:=UnitCommon.GetAnalyzerProperty(pcomm.Name, 'id');
              YhloCreateAssay(pbarcode, pserial, p_Analyzerid, messagerecord);
              pcomm.WriteText(messagerecord);
              state_send:='O';

        end;

     4:begin
              YhloCreateEnd(messagerecord);
              pcomm.WriteText(messagerecord);
              state_send:='L';
          end;
     5:begin
              pcomm.WriteText(#4);
              state_send:='eot';
          end;
     6:begin
              state:='';
              state_send:='';     //clear states
          end;

     end;
   //  memo2.Lines.Add('Status: '+state_send);



end;

Procedure YhloCreateRecord(p_fields:Tstrings; p_sep:char; var p_record:string); //generiranje na ASTM rekordi
var i:integer;
month, day:string;
begin
     p_record:='';
     for i:=0 to p_fields.Count-2 do
     p_record:=p_record+p_fields[i]+p_sep;
     p_record:=p_record+p_fields[p_fields.Count-1];
end;

Procedure YhloCreateHeader(var precord:string);
var i:integer;
    pfields:tstrings;
    month, day, hour, min, sec,rec, checksum:string;
begin
       pfields:=tstringlist.Create;
       pfields.add('1H');
       pfields.add('\^&');
       pfields.add('');
       pfields.add('');
       pfields.add('cxLis^^');
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('ASCII');
       pfields.add('QA');
       pfields.add('1394-97');
       month:=inttostr(Monthof(now));
       if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
       day:=inttostr(Dayof(now));
       if strtoint(day)<10 then day:='0'+day;
       hour:=inttostr(Hourof(now));
       if strtoint(hour)<10 then hour:='0'+hour;
       min:=inttostr(Minuteof(now));
       if strtoint(min)<10 then min:='0'+hour;
       sec:=inttostr(Secondof(now));
       if strtoint(sec)<10 then sec:='0'+sec;
       pfields.add(inttostr(Yearof(now))+month+day+hour+min+sec);
       YhloCreateRecord(pfields,'|', precord);
       precord:=precord+chr($0D)+chr($03);
       checksum:=Calculatechecksum(precord);
       precord:=chr($02)+precord+checksum+chr($0D)+chr($10);
end;

Procedure YhloCreatePatient(var precord:string);
var i:integer;
    checksum:string;
    pfields:tstrings;
begin
       pfields:=tstringlist.Create;
       pfields.add('2P');
       pfields.add(inttostr(serialno));
       pfields.add('');
       pfields.add(barcode);
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('^^');
       pfields.add('U');
       for i:=1 to 26 do pfields.add('');
       YhloCreateRecord(pfields,'|',precord);
       precord:=precord+chr($0D)+chr($03);
       checksum:=Calculatechecksum(precord);
       precord:=chr($02)+precord+checksum+chr($0D)+chr($10);
end;

Procedure YhloCreateAssay(pbarcode:string; pserial:integer; pAnalyzerid:string; var precord:string);

var i:integer;
    pfields, passays, passay_list:tstrings;
    display_msg, assays_field:string;
    month, day, hour, min, sec, checksum:string;
begin
       pfields:=tstringlist.Create;
       for i:=1 to 31 do
       pfields.add('');
       pfields[0]:='3O';
       pfields[1]:=inttostr(serialno);
       pfields[2]:='^';
       pfields[3]:=barcode;
       /////// del za zadavanje analizi
       passays:=tstringlist.Create;
       passay_list:=tstringlist.Create; //lista so aliasi za pacient od aparat
       dm1.getassays(pbarcode, pAnalyzerid , passay_list);  ///mesto direkten pristap do dataset od aliasi za pacient  od aparat
       display_msg:='ANALYZER='+panalyzer+'    SAMPLEID='+barcode+'    ASSAYS=[';
     {  dm1.qAssays.Close;
       dm1.qAssays.ParamByName('sampleID').value:=pbarcode;
       dm1.qAssays.Open;

       dm1.qAssays.First;
       i:=0;
       while not dm1.qAssays.Eof do     //vrtam za site aliasi sho se naznaceni za pacientot
       begin
            passays[i]:=dm1.qAssays.FieldByName('ALIASNAME').asstring+'^'+dm1.qAssays.FieldByName('ALIASNAME').asstring+'^'+'1.000000'+'^';
            display_msg:=display_msg+dm1.qAssays.FieldByName('ALIASNAME').asstring+', '; //se kreira stringot za prikaz vo memo
            dm1.qAssays.Next;
            inc(i);
       end; }

       for i:=0 to passay_list.Count-1 do     //vrtam za site aliasi sho se naznaceni za pacientot
       begin
            passays[i]:=passay_list[i]+'^'+passay_list[i]+'^'+'1.000000'+'^';
            display_msg:=display_msg+passay_list[i]+', '; //se kreira stringot za prikaz vo memo
       end;

       display_msg:=copy(display_msg,1,length(display_msg)-2)+']';    //ispis koi analizi se prateni kon analajzer
       FrmMain.SetTextColor('blue');
       frmMain.Memo1.Lines.Add(display_msg);
       FrmMain.SetTextColor('black');

       YhloCreateRecord(passays, '\',assays_field);
       pfields[4]:=assays_field;
       /////////////
       pfields[5]:='R';

       month:=inttostr(Monthof(now));
       if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
       day:=inttostr(Dayof(now));
       if strtoint(day)<10 then day:='0'+day;
       hour:=inttostr(Hourof(now));
       if strtoint(hour)<10 then hour:='0'+hour;
       min:=inttostr(Minuteof(now));
       if strtoint(min)<10 then min:='0'+hour;
       sec:=inttostr(Secondof(now));
       if strtoint(sec)<10 then sec:='0'+sec;
       pfields[6]:=inttostr(Yearof(now))+month+day+hour+min+sec;
       pfields[7]:=inttostr(Yearof(now))+month+day+hour+min+sec;
       pfields[9]:='1.0';
       pfields[15]:='serum';
       pfields[25]:='Q';
       YhloCreateRecord(pfields,'|',precord);
       precord:=precord+chr($0D)+chr($03);
       checksum:=Calculatechecksum(precord);
       precord:=chr($02)+precord+checksum+chr($0D)+chr($10);
end;

Procedure YhloCreateEnd(var precord:string);
var i:integer;
    pfields:tstrings;
    checksum:string;
begin
       pfields:=tstringlist.Create;
       pfields.add('4L');
       pfields.add(inttostr(serialno));
       pfields.add('N');
       YhloCreateRecord(pfields,'|',precord);
       precord:=precord+chr($0D)+chr($17);
       checksum:=Calculatechecksum(precord);
       precord:=chr($02)+precord+checksum+chr($0D)+chr($10);
end;

end.
