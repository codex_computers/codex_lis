unit UnitPacienti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls, System.Actions, Vcl.ActnList, Vcl.Buttons,
  Vcl.DBCtrls, Vcl.ComCtrls;

type
  TFrmPacienti = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    Button1: TButton;
    ActionList1: TActionList;
    aExit: TAction;
    DBGrid3: TDBGrid;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBText5: TDBText;
    Label7: TLabel;
    Label8: TLabel;
    Panel4: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    SpeedButton7: TSpeedButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Panel5: TPanel;
    ProgressBar1: TProgressBar;
    Label11: TLabel;
    aSend: TAction;
    aSendAll: TAction;
    aChangeVal: TAction;
    aDelete: TAction;
    aDeleteAll: TAction;
    aDeletefull: TAction;
    aSendFull: TAction;
    Label12: TLabel;
    DBText6: TDBText;
    DBText7: TDBText;
    procedure aExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure aSendExecute(Sender: TObject);
    procedure aDeletefullExecute(Sender: TObject);
    procedure Panel4Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPacienti: TFrmPacienti;

implementation
 uses dm;
{$R *.dfm}

procedure TFrmPacienti.aDeletefullExecute(Sender: TObject);
begin
     if MessageDlg('��������! �� ����� �� ���� ����� �� �� �������� ���� ����������� ��������� ��� ���. ���� ������ �� ����������?',
     mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes then
     begin
         dm1.resultsbuffer.Clear;
         dm1.PatientsBuffer.Clear;
     end;
end;

procedure TFrmPacienti.aExitExecute(Sender: TObject);
begin
     frmPacienti.Close;
end;

procedure TFrmPacienti.aSendExecute(Sender: TObject);
begin
 //prefrlanje podatoci poedinecno
{     if (dm1.qresults.Locate('ALIASNAME',dm1.resultsbuffertest.Value,[])) then
     begin
       try
         dm1.qresults.edit;
         dm1.qresultsREZULTAT.Value:=dm1.resultsbufferresult.Value;
         dm1.qresults.Post;
         dm1.resultsbuffer.Delete;
       except
       dm1.qresults.Cancel;
       end;
     end
     else showmessage('�� ��������� �� � �������� ��� ��������� � �� ���� �� �� �������!');  }
     if (dm1.dsResults.DataSet.Locate('ALIASNAME',dm1.resultsbuffertest.Value,[])) then
     begin
       try
         dm1.dsResults.DataSet.edit;
         dm1.dsResults.DataSet.FieldByName('REZULTAT').Value:=dm1.resultsbufferresult.Value;
         dm1.dsResults.DataSet.Post;
         dm1.resultsbuffer.Delete;
       except
       dm1.dsResults.DataSet.Cancel;
       end;
     end
     else showmessage('�� ��������� �� � �������� ��� ��������� � �� ���� �� �� �������!');
end;

procedure TFrmPacienti.BitBtn1Click(Sender: TObject);
begin
     if panel4.Visible then panel4.Visible:=false else
     begin
         panel4.visible:=true;
         edit1.Text:=dm1.PatientsBuffersampleID.Value;
         edit2.SetFocus;
         panel4.Tag:=2;
     end;
end;

procedure TFrmPacienti.BitBtn2Click(Sender: TObject);
var pac, prefpac:integer;
begin
     if MessageDlg('��������! �� ����� �� ���� ����� �� �� �������� ���� ����������� ��������� ��� ���. ���� ������ �� ����������?',
     mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes then
     begin
        dm1.patientsbuffer.First;
        panel5.Visible:=true;
        progressbar1.Min:=0;
        progressbar1.Max:=dm1.PatientsBuffer.RecordCount;
        while not dm1.patientsbuffer.eof do
        begin
         if not (dm1.dsResults.DataSet.IsEmpty) then
         begin
          dm1.resultsbuffer.First;
          while not dm1.resultsbuffer.Eof do
          begin
          if (dm1.dsResults.DataSet.Locate('ALIASNAME',dm1.resultsbuffertest.Value,[])) then
          begin
            try
              dm1.dsResults.DataSet.edit;
              dm1.dsResults.DataSet.FieldByName('REZULTAT').Value:=dm1.resultsbufferresult.Value;
              dm1.dsResults.DataSet.Post;
              dm1.resultsbuffer.Delete;
            except
                 begin
                    dm1.dsResults.DataSet.Cancel;
                    dm1.resultsbuffer.Next;
                 end;
            end;
          end
          else
          begin
               // showmessage('�� ��������� �� � �������� ����������� '+dm1.resultsbuffertest.Value+' � �� ���� �� �� �������!');
                dm1.resultsbuffer.Next;
          end;
         end;
     end;
     dm1.PatientsBuffer.Next;
     progressbar1.StepBy(1);
    end;
    panel5.Visible:=false;
  end;

 {    if MessageDlg('��������! �� ����� �� ���� ����� �� �� �������� ���� ����������� ��������� ��� ���. ���� ������ �� ����������?',
     mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrYes then
     begin
        dm1.patientsbuffer.First;
        panel5.Visible:=true;
        progressbar1.Min:=0;
        progressbar1.Max:=dm1.PatientsBuffer.RecordCount;
        while not dm1.patientsbuffer.eof do
        begin
         if not (dm1.qresults.IsEmpty) then
         begin
          dm1.resultsbuffer.First;
          while not dm1.resultsbuffer.Eof do
          begin
          if (dm1.qresults.Locate('ALIASNAME',dm1.resultsbuffertest.Value,[])) then
          begin
            try
              dm1.qresults.edit;
              dm1.qresultsREZULTAT.Value:=dm1.resultsbufferresult.Value;
              dm1.qresults.Post;
              dm1.resultsbuffer.Delete;
            except
                 begin
                    dm1.qresults.Cancel;
                    dm1.resultsbuffer.Next;
                 end;
            end;
          end
          else
          begin
               // showmessage('�� ��������� �� � �������� ����������� '+dm1.resultsbuffertest.Value+' � �� ���� �� �� �������!');
                dm1.resultsbuffer.Next;
          end;
         end;
     end;
     dm1.PatientsBuffer.Next;
     progressbar1.StepBy(1);
    end;
    panel5.Visible:=false;
  end; }
end;

procedure TFrmPacienti.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if (column.Field.FieldName='REZULTAT') and  (column.Field.Value<>'') then
     DBGrid2.Canvas.Font.Color := clRed;
     DBGrid2.DefaultDrawDataCell(Rect, column.Field, State);
end;

procedure TFrmPacienti.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dm1.dsPatients.DataSet.Close;
     dm1.dsResults.DataSet.Close;
end;

procedure TFrmPacienti.FormShow(Sender: TObject);
begin
     dm1.dsPatients.DataSet.Open;
     dm1.dsResults.DataSet.Open;

end;

procedure TFrmPacienti.Panel4Exit(Sender: TObject);
begin
     edit2.Text:='';
end;

procedure TFrmPacienti.SpeedButton2Click(Sender: TObject);
begin   //prefrlanje na rezultati za sampleID
   if dm1.dsResults.DataSet.IsEmpty  then showmessage('�� ��������� �� �� ��������� ������� �������� �������')
   else
   begin
     dm1.resultsbuffer.First;
     while not dm1.resultsbuffer.Eof do
     begin
         if (dm1.dsResults.DataSet.Locate('ALIASNAME',dm1.resultsbuffertest.Value,[])) then
         begin
            try
              dm1.dsResults.DataSet.edit;
              dm1.dsResults.DataSet.FieldByName('REZULTAT').Value:=dm1.resultsbufferresult.Value;
              dm1.dsResults.DataSet.Post;
              dm1.resultsbuffer.Delete;
            except
            begin
                 dm1.dsResults.DataSet.Cancel;
                 dm1.resultsbuffer.Next;
            end;
            end;
         end
         else
         begin
             showmessage('�� ��������� �� � �������� ����������� '+dm1.resultsbuffertest.Value+' � �� ���� �� �� �������!');
             dm1.resultsbuffer.Next;
         end;
     end;
   end;
end;

procedure TFrmPacienti.SpeedButton3Click(Sender: TObject);
begin
     if panel4.Visible then panel4.Visible:=false else
     begin
         panel4.visible:=true;
         edit1.Text:=dm1.resultsbufferresult.Value;
         edit2.SetFocus;
         panel4.Tag:=0;
     end;
end;

procedure TFrmPacienti.SpeedButton4Click(Sender: TObject);
begin
     dm1.resultsbuffer.Delete;      //brisenje rezultat poedinecno
end;

procedure TFrmPacienti.SpeedButton5Click(Sender: TObject);
var id:ansistring;     // brisenje rezultati za sampleID
begin
     id:=dm1.PatientsBuffersampleID.Value;
     dm1.resultsbuffer.First;
     while (dm1.resultsbuffer.RecordCount>0) and (dm1.resultsbuffersampleID.value=id) do
     dm1.resultsbuffer.Delete;
end;

procedure TFrmPacienti.SpeedButton7Click(Sender: TObject);
begin
   if panel4.Tag=0 then
   begin
     dm1.resultsbuffer.Edit;
     dm1.resultsbufferresult.Value:=edit2.Text;
     dm1.resultsbuffer.Post;
     panel4.visible:=false;
     DbGrid3.SetFocus;
   end;
   if panel4.Tag=2 then
   begin
     try
         dm1.resultsbuffer.First;
        while not (dm1.resultsbuffer.eof) and (dm1.resultsbuffersampleID.value=edit1.text) do
        begin
            dm1.resultsbuffer.Edit;
            dm1.resultsbuffersampleID.value:=edit2.text;
            dm1.resultsbuffer.Post;
           // dm1.resultsbuffer.next;
        end;
        if (dm1.PatientsBuffer.Locate('sampleID',edit2.text,[])) then  //ako se menja ID i vekje postoe takvo da nema dupliranje
        begin
            if (dm1.PatientsBuffer.Locate('sampleID',edit1.text,[])) then
            dm1.PatientsBuffer.Delete;          //ako postoe se brise
        end
        else
        begin
            dm1.PatientsBuffer.Edit;        // ako ne postoe se menja vo novata vrednost
            dm1.patientsbuffersampleID.Value:=edit2.Text;
            dm1.patientsbuffer.Post;
            dm1.resultsbuffer.Refresh;
        end;
        DBGrid1.SetFocus;
     finally
        panel4.visible:=false;
     end;

   end;
end;

end.
