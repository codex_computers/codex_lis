object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 585
  ClientWidth = 844
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 81
    Width = 825
    Height = 135
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 360
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Send ENQ'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Memo2: TMemo
    Left = 8
    Top = 222
    Width = 825
    Height = 218
    Lines.Strings = (
      'Memo2')
    TabOrder = 2
  end
  object Button2: TButton
    Left = 360
    Top = 50
    Width = 75
    Height = 25
    Caption = 'Send EOT'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 441
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Send STX'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 441
    Top = 50
    Width = 75
    Height = 25
    Caption = 'Send ETX'
    TabOrder = 5
    OnClick = Button4Click
  end
  object RadioButton1: TRadioButton
    Left = 32
    Top = 16
    Width = 65
    Height = 17
    Caption = 'Sender'
    TabOrder = 6
  end
  object RadioButton2: TRadioButton
    Left = 32
    Top = 40
    Width = 113
    Height = 17
    Caption = 'Receiver'
    Checked = True
    TabOrder = 7
    TabStop = True
  end
  object Memo3: TMemo
    Left = 11
    Top = 446
    Width = 825
    Height = 135
    Lines.Strings = (
      'H|\^&||PSWD|CxLis|||||Maglumi||P|E1394-97|20100319'
      'P|1||||Nakje Micev|||F'
      'O|1|1234567||^^^TSH|R'
      'O|1|1234567||^^^FT3|R'
      'O|1|1234567||^^^FT4|R'
      'L|1|N')
    TabOrder = 8
  end
  object Button5: TButton
    Left = 249
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Calculate CS'
    TabOrder = 9
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 600
    Top = 19
    Width = 75
    Height = 25
    Caption = 'showCHR'
    TabOrder = 10
    OnClick = Button6Clic
  end
  object Edit1: TEdit
    Left = 600
    Top = 50
    Width = 121
    Height = 21
    TabOrder = 11
  end
  object Button7: TButton
    Left = 249
    Top = 50
    Width = 75
    Height = 25
    Caption = 'Send CR'
    TabOrder = 12
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 168
    Top = 50
    Width = 75
    Height = 25
    Caption = 'Send LF'
    TabOrder = 13
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 168
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Send msg'
    TabOrder = 14
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 681
    Top = 19
    Width = 75
    Height = 25
    Caption = 'Convert'
    TabOrder = 15
    OnClick = Button10Click
  end
  object VaWaitMessage1: TVaWaitMessage
    OnMessage = VaWaitMessage1Message
    Active = False
    Left = 480
    Top = 120
  end
  object VaWaitMessage2: TVaWaitMessage
    OnMessage = VaWaitMessage1Message
    Active = False
    Left = 672
    Top = 152
  end
  object VaCapture1: TVaCapture
    MaxMsgLen = 2048
    Active = False
    Left = 552
    Top = 136
  end
  object VaComm1: TVaComm
    Baudrate = Br9600
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = DtrDisabled
    FlowControl.ControlRts = RtsDisabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    PortNum = 6
    DeviceName = 'COM6'
    MonitorEvents = [CeError, CeRxChar]
    OnRxChar = VaComm1RxChar
    Version = '1.5.2.0'
    Left = 544
    Top = 240
  end
end
