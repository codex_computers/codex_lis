unit UnitMinividas;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;
 procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
 Procedure minividas_parse(data:ansistring);
const
  pAnalyzer:string='Minividas'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm;

procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
var
  I, j: Integer;
  Fmessage: string;
begin

 for I := 1 to Length(data) do
     case data[I] of
      chr($0D):  //Waiting for this?
        begin
          minividas_parse(FMessage);
         // memo2.Lines.Add(fmessage);
          FMessage := '';  //reset received message
        end;
      else
        FMessage := FMessage + data[I];
     end;

end;


Procedure minividas_parse(data:ansistring);
  var  position, i,j:integer;
      samp_id, alias, qtvalue,qnvalue, qresult, qunit :string;
      work_data:string;
      splitstrings:tstrings;
begin
     splitstrings := TStringlist.Create;
     for i:=1 to length(data) do   //vo slucaj da ima prasalnici da gi trgne
      begin
            if data[i]<>'?' then work_data:=work_data+data[i];
      end;
       position:=pos('ci', work_data);
       if position>0 then
       begin
          j:=position+2;
          while (work_data[j]<>'|') do
          begin
                samp_id:=samp_id+work_data[j];
                inc(j);
          end;
       end;
       position:=pos('rt', work_data);
       if position>0 then
       begin
          j:=position+2;
          while (work_data[j]<>'|') do
          begin

                alias:=alias+work_data[j];
                inc(j);
          end;
       end;
       position:=pos('qn', work_data);
       if position>0 then
       begin
          j:=position+2;
          while (work_data[j]<>'|') do
          begin
                qnvalue:=qnvalue+work_data[j];
                inc(j);
          end;
          if length(qnvalue)>0 then
          begin
               dm1.SplitString(qnvalue, #32, splitstrings);
               qresult:=splitstrings[0];
               if splitstrings.Count>1  then  qunit:=splitstrings[1];
              //   qresult:=qnvalue;
          end;
       end;
       position:=pos('ql', work_data);
       if position>0 then
       begin
          j:=position+2;
          while (work_data[j]<>'|') do
          begin
                qtvalue:=qtvalue+work_data[j];
                inc(j);
          end;
          if qtvalue<>'' then qtvalue:='-'; //ako e prazno go polnam so tire za da znam

       end;
      // zapis vo buffer datasets
      if (samp_id<>'') and (alias<>'') then //proverka ako ima alias i sample id da zapisuva
      begin
          if not (dm1.PatientsBuffer.Locate('sampleID',samp_id,[])) then
           begin
                dm1.PatientsBuffer.Append;   //zapis na barkod dokolku ne postoi vo buffer dataset
                dm1.PatientsBuffersampleID.Value:=samp_id;
                dm1.PatientsBuffer.Post;
           end;

           FrmMain.memo1.Lines.Add('     ANALYZER='+panalyzer+'    SAMPLEID='+samp_id+'    TEST='+alias+'   '+'RESULT='+qnvalue+'(n) '+qtvalue+'(q)'+'    VALUE='+qresult+'    UNIT='+qunit);
           dm1.resultsbuffer.Append;
           dm1.resultsbufferSampleid.Value:=samp_id;
           if qresult<>'' then dm1.resultsbufferresult.Value:=trim(qresult) //ako e kvantitativen rezultat prazen, da zapise kvalitativen
           else dm1.resultsbufferresult.Value:=qtvalue;
           dm1.resultsbuffertest.value:=alias;
           dm1.resultsbufferunit.value:=qunit;
           dm1.resultsbuffer.Post;

      end;

end;
end.
