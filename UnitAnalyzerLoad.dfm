object frmAnalyzerLoad: TfrmAnalyzerLoad
  Left = 0
  Top = 0
  Caption = #1047#1072#1076#1072#1074#1072#1114#1077' '#1088#1072#1073#1086#1090#1085#1072' '#1083#1080#1089#1090#1072' '#1085#1072' '#1072#1085#1072#1083#1072#1112#1079#1077#1088
  ClientHeight = 383
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = 18
  Font.Name = 'Calibri'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 679
    Height = 113
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 8
      Top = 57
      Width = 144
      Height = 48
      Action = aPatLoad
      Caption = #1048#1079#1073#1077#1088#1080' '#1087#1072#1094#1080#1077#1085#1090#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 16
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn3: TBitBtn
      Left = 560
      Top = 57
      Width = 109
      Height = 48
      Action = aExit
      Caption = '           '#1048#1079#1083#1077#1079'             (Esc)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 16
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      WordWrap = True
    end
    object BitBtn4: TBitBtn
      Left = 445
      Top = 57
      Width = 109
      Height = 48
      Caption = '   '#1048#1089#1095#1080#1089#1090#1080' '#1083#1080#1089#1090#1072'    (F8)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = 16
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      WordWrap = True
      StyleElements = [seClient, seBorder]
      OnClick = BitBtn4Click
    end
    object BitBtn2: TBitBtn
      Left = 158
      Top = 59
      Width = 143
      Height = 48
      Caption = #1047#1072#1076#1072#1076#1080' '#1082#1086#1085' '#1072#1085#1072#1083#1072#1112#1079#1077#1088' (F9)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = 16
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = True
      StyleElements = [seClient, seBorder]
      OnClick = BitBtn2Click
    end
    object RadioGroup1: TRadioGroup
      Left = 2
      Top = 2
      Width = 675
      Height = 49
      Align = alTop
      Caption = #1044#1086#1089#1090#1072#1087#1085#1080' '#1072#1085#1072#1083#1072#1112#1079#1077#1088#1080
      Columns = 2
      TabOrder = 4
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 364
    Width = 679
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = #1047#1072#1076#1072#1076#1077#1085#1080' '#1087#1072#1094#1080#1077#1085#1090#1080':'
  end
  object Memo1: TMemo
    Left = 0
    Top = 113
    Width = 679
    Height = 251
    Align = alClient
    TabOrder = 2
  end
  object ActionList1: TActionList
    Left = 592
    Top = 104
    object aExit: TAction
      Caption = '              '#1048#1079#1083#1077#1079'               (Esc)'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aExitExecute
    end
    object aClear: TAction
      Caption = '   '#1048#1089#1095#1080#1089#1090#1080' '#1083#1080#1089#1090#1072'    (F8)'
      ShortCut = 119
    end
    object aLoad: TAction
      Caption = #1047#1072#1076#1072#1076#1080' '#1082#1086#1085' '#1072#1085#1072#1083#1072#1112#1079#1077#1088' (F9)'
    end
    object aPatLoad: TAction
      Caption = #1048#1079#1073#1077#1088#1080' '#1087#1072#1094#1080#1077#1085#1090#1080
      OnExecute = aPatLoadExecute
    end
  end
  object tAnalyzers: TVirtualTable
    Left = 592
    Top = 160
    Data = {04000000000000000000}
    object tAnalyzersid: TIntegerField
      FieldName = 'id'
    end
    object tAnalyzersprotocol: TStringField
      FieldName = 'protocol'
    end
    object tAnalyzerscomm: TStringField
      FieldName = 'comm'
      Size = 10
    end
    object tAnalyzersanalyzerid: TIntegerField
      FieldName = 'analyzerid'
    end
  end
end
