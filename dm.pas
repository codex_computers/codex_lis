unit dm;

interface

uses
  System.SysUtils, System.Classes,OraCall, Data.DB, DBAccess, Ora, MemDS, Vcl.Dialogs,
  VirtualTable, OraErrHand, FireDAC.Stan.Intf, FireDAC.Stan.Option, System.StrUtils,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  FireDAC.Comp.BatchMove, FireDAC.Comp.BatchMove.Text;



type
  Tdm1 = class(TDataModule)

    dsresbuffer: TDataSource;
    resultsbuffer: TVirtualTable;
    resultsbufferresult: TStringField;
    resultsbuffertest: TStringField;
    resultsbufferdeviceid: TStringField;
    qpatients: TOraQuery;
    qpatientsBRDNEVN: TIntegerField;
    qpatientsGOD1: TIntegerField;
    qpatientsBARKOD: TStringField;
    qpatientsEMBG: TFloatField;
    qpatientsDATUM_ROD: TDateTimeField;
    qpatientsPACIENT: TStringField;
    qpatientsREID: TIntegerField;
    dsPatients: TOraDataSource;
    dsResults: TOraDataSource;
    qresults: TOraQuery;
    OraSession: TOraSession;
    qresultsBRDNEVN: TIntegerField;
    qresultsREID: TIntegerField;
    qresultsGOD1: TIntegerField;
    qresultsID: TFloatField;
    qresultsREZULTAT: TStringField;
    qresultsNAZREZULT: TStringField;
    qresultsPODREDUVANJE: TFloatField;
    qresultsREF_MIN: TFloatField;
    qresultsREF_MAX: TFloatField;
    qresultsMERNA_ED: TStringField;
    qresultsREF_VREDNOST: TStringField;
    qresultsALIASNAME: TStringField;
    resultsbuffersource_result: TStringField;
    PatientsBuffer: TVirtualTable;
    dsPatBuffer: TDataSource;
    PatientsBuffersampleID: TStringField;
    resultsbuffersampleID: TStringField;
    qpatientsPOL: TStringField;
    qpatientsPRIMEN: TDateTimeField;
    OraErrorHandler1: TOraErrorHandler;
    resultsbufferunit: TStringField;
    FBconn: TFDConnection;
    fqpatients: TFDQuery;
    fqresults: TFDQuery;
    FDTransaction1: TFDTransaction;
    fqpatientsBRDNEVN: TIntegerField;
    fqpatientsGOD1: TSmallintField;
    fqpatientsREID: TIntegerField;
    fqpatientsEMBG: TFloatField;
    fqpatientsDATUM_ROD: TDateField;
    fqpatientsPACIENT: TStringField;
    fqpatientsPOL: TStringField;
    fqpatientsBARKOD: TStringField;
    fqpatientsPRIMEN: TDateField;
    fqresultsBRDNEVN: TIntegerField;
    fqresultsREID: TIntegerField;
    fqresultsGOD1: TSmallintField;
    fqresultsID: TIntegerField;
    fqresultsREZULTAT: TStringField;
    fqresultsNAZREZULT: TStringField;
    fqresultsPODREDUVANJE: TIntegerField;
    fqresultsREF_MIN: TSingleField;
    fqresultsREF_MAX: TSingleField;
    fqresultsMERNA_ED: TStringField;
    fqresultsREF_VREDNOST: TStringField;
    fqresultsALIASNAME: TStringField;
    fAssays: TFDQuery;
    qAssays: TOraQuery;
    qQuerytext: TOraQuery;
    qQuerytextID: TFloatField;
    qQuerytextMETHODNAME: TStringField;
    qQuerytextSQLTEXT: TStringField;
    fQuerytext: TFDQuery;
    fQuerytextID: TIntegerField;
    fQuerytextMETHODNAME: TStringField;
    fQuerytextSQLTEXT: TStringField;
    dsQuerytext: TDataSource;
    dsAssays: TDataSource;
    fAnalyzers: TFDQuery;
    qAnalyzers: TOraQuery;
    dsAnalyzers: TDataSource;
    qPatientList: TOraQuery;
    fPatientList: TFDQuery;
    dsPatientList: TDataSource;
    AssaysToLoad: TVirtualTable;
    AssaysToLoadid: TIntegerField;
    AssaysToLoadbarcode: TStringField;
    AssaysToLoadassay: TStringField;
    AssaysToLoadpatientID: TStringField;
    AssaysToLoadpatientName: TStringField;
    fGetPatient: TFDQuery;
    qGetPatient: TOraQuery;
    dsGetPatient: TDataSource;
    AssaysToLoadsex: TStringField;
    AssaysToLoaddob: TDateField;
    AssaysToLoadmessage: TStringField;
    mAnalyzers: TFDMemTable;
    mAnalyzerscomm_reference: TStringField;
    mAnalyzersanalyzer_protocol: TStringField;
    mAnalyzersanalyzer_id: TStringField;
    mAnalyzerscomport: TStringField;
    mAnalyzersbaud: TStringField;
    mAnalyzersdata: TStringField;
    mAnalyzersparity: TStringField;
    mAnalyzersstop: TStringField;
    mAnalyzershandflow: TStringField;
    mAnalyzerscommunication_type: TStringField;
    mAnalyzersfile_path: TStringField;
    mAnalyzersfile_load_path: TStringField;
    mAnalyzersfile_type: TStringField;
    mAnalyzersmanual_load: TStringField;
    fUpdateqresults: TFDUpdateSQL;
    qAliases: TOraQuery;
    fAliases: TFDQuery;
    dsAliases: TDataSource;
    procedure resultsbufferAfterScroll(DataSet: TDataSet);
    procedure resultsbufferAfterDelete(DataSet: TDataSet);
    procedure OraErrorHandler1Error(Sender: TObject; E: Exception;
      ErrorCode: Integer; const ConstraintName: string; var Msg: string);
    procedure dsPatBufferDataChange(Sender: TObject; Field: TField);
    function IScan(ch: Char; const S: string; fromPos: Integer): Integer;
    procedure SplitString(const S: string; separator: Char; substrings: TStrings);
    procedure FBconnError(ASender, AInitiator: TObject;
      var AException: Exception);
    Procedure getassays(pbarcode, panalyzer:string; var passays:tstrings);
    procedure PatListOpen(panalyzer:string);  //lista na pacienti so neizvrseni analizi po aparat, za manual load aparati
    procedure GetPatientOpen(pbarcode:string); //vrakja podatoci za pacientot za da se predadat na analajzer
  private

    { Private declarations }
  public
     ws_id:string;
     instrumnt_id:string;
     tip_db:string;
     procedure ora_setqueries;
     Procedure fb_setqueries;


    { Public declarations }
  end;

var
  dm1: Tdm1;


implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure Tdm1.FBconnError(ASender, AInitiator: TObject;
  var AException: Exception);
begin
     showmessage(aexception.Message);

end;

function Tdm1.IScan(ch: Char; const S: string; fromPos: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := fromPos to Length(S) do
  begin
    if S[i] = ch then
    begin
      Result := i;
      Break;
    end;
  end;
end;

procedure Tdm1.SplitString(const S: string; separator: Char; substrings: TStrings);
var
  i, n: Integer;
begin
  if Assigned(substrings) and (Length(S) > 0) then
  begin
    i := 1;
    repeat
      n := dm1.IScan(separator, S, i);
      if n = 0 then
        n := Length(S) + 1;
      substrings.Add(Copy(S, i, n - i));
      i := n + 1;
    until
      i > Length(S);
  end;
end;



procedure Tdm1.dsPatBufferDataChange(Sender: TObject; Field: TField);
begin
     if dm1.qresults.IsEmpty then      //flag za da znam koga se klika na prefrlanje ako nema kade da prefrle voopsto da ne probuva
        dm1.dsPatBuffer.Tag:=1
     else
        dm1.dsPatBuffer.Tag:=0;
end;

procedure Tdm1.OraErrorHandler1Error(Sender: TObject; E: Exception;
  ErrorCode: Integer; const ConstraintName: string; var Msg: string);
begin
     showmessage(e.Message);
end;

procedure Tdm1.resultsbufferAfterDelete(DataSet: TDataSet);
begin
     if dm1.resultsbuffer.RecordCount=0 then dm1.PatientsBuffer.Delete;   //breisenje na sampleid ako site rezultati za nego se izbrisani
end;

procedure Tdm1.resultsbufferAfterScroll(DataSet: TDataSet);
begin
   //  resultsbuffer.Filtered:=false;
  //   resultsbuffer.Filter:='sampleID='+dm1.PatientsBufferSampleid.Value;
  //   resultsbuffer.Filtered:=true;
end;

Procedure Tdm1.ora_setqueries;
begin
     dm1.qAliases.Open;
     dm1.qQuerytext.open;
     if (dm1.qQuerytext.Locate('METHODNAME','cxlis_getassays',[])) then
     begin
          dm1.qAssays.SQL.Text:=dm1.qQuerytextSQLTEXT.Text;
          { dm1.qAssays.Close;
           dm1.qAssays.ParamByName('sampleID').value:='10000028320';
           dm1.qAssays.Open;
           while not dm1.qAssays.Eof do
           begin
           showmessage(dm1.qAssays.FieldByName('ALIASNAME').AsString);
           dm1.qAssays.Next;
           end }    //ova bese za test dali ke raborte so dybamic query
     end;
     if (dm1.qQuerytext.Locate('METHODNAME','cxlis_getpatientlist',[])) then
     begin
          dm1.qPatientList.SQL.Text:=dm1.qQuerytextSQLTEXT.Text;
     end;
     if (dm1.qQuerytext.Locate('METHODNAME','cxlis_getpatient',[])) then
     begin
          dm1.qGetPatient.SQL.Text:=dm1.qQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_qresultsupdate',[])) then
     begin
          dm1.qResults.SQLUpdate.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
end;
Procedure Tdm1.fb_setqueries;
begin
     dm1.fAliases.Open;
     dm1.fQuerytext.open;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_getassays',[])) then
     begin
          dm1.fAssays.SQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_getpatientlist',[])) then
     begin
          dm1.fPatientList.SQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_getpatient',[])) then
     begin
          dm1.fGetPatient.SQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_qpatients',[])) then
     begin
          dm1.fqPatients.SQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_qresults',[])) then
     begin
          dm1.fqResults.SQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;
     if (dm1.fQuerytext.Locate('METHODNAME','cxlis_qresultsupdate',[])) then
     begin
          dm1.fQuerytext.UpdateObject:=dm1.fupdateqresults;
          dm1.fupdateqresults.ModifySQL.Text:=dm1.fQuerytextSQLTEXT.Text;
     end;

end;
Procedure Tdm1.getassays(pbarcode, panalyzer:string; var passays:tstrings);  //zemanje aliasi od baza za barkod
var
   i:integer;
   assays:tstrings;
begin
     Assays:=tstringlist.Create;
     dm1.dsAssays.DataSet.Close;
     if dm1.tip_db='oracle' then
      begin
           dm1.qAssays.ParamByName('sampleID').value:=pbarcode;
           dm1.qAssays.ParamByName('instrID').value:=panalyzer;
      end;
      if dm1.tip_db='firebird' then
      begin
           dm1.fAssays.ParamByName('sampleID').value:=pbarcode;
           dm1.fAssays.ParamByName('instrID').value:=panalyzer;
      end;
      dm1.dsAssays.DataSet.Open;
      dm1.dsAssays.DataSet.First;
      while not dm1.dsAssays.DataSet.Eof do
      begin
            assays.Add(dm1.dsAssays.Dataset.FieldByName('ALIASNAME').asstring);
            dm1.dsAssays.DataSet.Next;
      end;
      passays:=assays;
      //assays.Free;
end;
procedure Tdm1.PatListOpen(panalyzer:string);
begin
     dm1.dsPatientList.DataSet.Close;
     if dm1.tip_db='oracle' then
      begin
           dm1.qPatientList.ParamByName('instrID').asinteger:=strtoint(panalyzer);
      end;
      if dm1.tip_db='firebird' then
      begin
           dm1.fPatientList.ParamByName('instrID').asinteger:=strtoint(panalyzer);

      end;
      dm1.dsPatientList.DataSet.Open;

end;
procedure Tdm1.GetPatientOpen(pbarcode:string);
begin
     dm1.dsGetPatient.DataSet.Close;
     if dm1.tip_db='oracle' then
      begin
           dm1.qGetPatient.ParamByName('sampleID').value:=pbarcode;
      end;
      if dm1.tip_db='firebird' then
      begin
           dm1.fGetPatient.ParamByName('sampleID').value:=pbarcode;
      end;
      dm1.dsGetPatient.DataSet.Open;

end;


end.
