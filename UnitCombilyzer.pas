unit UnitCombilyzer;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;
 procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
 Procedure combilyzer_parse(msg:ansistring);
 Procedure Combilyzer_process_result(test, res: ansistring; var value, units, tag:ansistring);

var
  barcode,daily_id:string;
const
  pAnalyzer:string='Combilyzer'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm;

procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
var
  I, j: Integer;
  Fmessage: string;
begin

 for I := 1 to Length(data) do
    case data[I] of
      #10:; //skip this one
      #13:  //Waiting for this?
        begin
          Combilyzer_parse(FMessage);
          FMessage := '';  //reset received message
        end;
      else //not #10 or #13
        FMessage := FMessage + data[I];
    end;
end;


Procedure combilyzer_parse(msg:ansistring);
 var i:integer;
    datum, id, test, rezult, prefix, value, units, tag:ansistring;
begin
 datum:='';
 id:='';
 test:='';
 rezult:='';
 value:='';
 units:='';
 tag:='';

 if length(msg)>3 then
 begin
   prefix:=msg[2]+msg[3]+msg[4];
   if (prefix ='ID:') or (Prefix='No.') then
   begin
        for  i:=5 to length(msg) do id:=id+msg[i];
        if (prefix ='ID:') then
        begin
              if trim(id)<>'' then
              begin
                   barcode:=trim(id);
                   FrmMain.memo1.Lines.Add('   ANALYZER='+panalyzer+'   SampleID: '+barcode);
              end
              else FrmMain.memo1.Lines.Add('*** ID: ������! ���� ID �� ��������! ������� ���� �� ������� ������ ID');

              if not (dm1.PatientsBuffer.Locate('sampleID',barcode,[])) then
              begin
                   dm1.PatientsBuffer.edit;      // ako se koristi barkod citac so ova dnevniot broj na proba se zamenuva so barkodot
                   dm1.PatientsBuffersampleID.Value:=barcode;
                   dm1.PatientsBuffer.Post;
              end
              else
              begin
                  if (dm1.PatientsBuffer.Locate('sampleID',daily_id,[])) and (daily_id<>barcode) then
                  dm1.PatientsBuffer.Delete;  //granicen slucaj ako se prefrla ist barkod dva pati, da ne stoe i dneven brojac
              end;
        end
        else
        begin
              FrmMain.memo1.Lines.Add('   ANALYZER='+panalyzer+'    Daily No: '+id);
              daily_id:=trim(id);
              barcode:=daily_id; //inicijalizacija ako nema barkod da bide popolneto so dneven id i da znaat deka treba da go promenat
              dm1.PatientsBuffer.Append;   // se vnesuva prvo so dneven broj, za vo slucaj da e isklucen barkod citacot
              dm1.PatientsBuffersampleID.Value:=daily_id;
              dm1.PatientsBuffer.Post;
        end;
   end
   else
   if (prefix<>'Dat')   then
      begin
           test:=trim(msg[2]+msg[3]+msg[4]);
           for i:=6 to 22 do rezult:=rezult+msg[i];
           if test<>'RT' then
           begin
                Combilyzer_process_result(test, rezult, value, units, tag);
                FrmMain.memo1.Lines.Add('     ANALYZER='+panalyzer+'    TEST='+test+'   '+'RESULT='+rezult+'   TAG='+tag+'   VALUE='+value+'   UNIT='+units);
                dm1.resultsbuffer.Append;
                dm1.resultsbufferSampleid.Value:=barcode;
                dm1.resultsbufferresult.Value:=trim(tag+'  '+value);
                dm1.resultsbuffertest.value:=test;
                dm1.resultsbufferunit.value:=units;
                dm1.resultsbuffer.Post;
           end;
      end
   else
      begin
           for  i:=7 to 22 do datum:=datum+msg[i];
           FrmMain.memo1.Lines.Add('  ');
           FrmMain.memo1.Lines.Add('Date: '+datum);
      end;
 end;

end;

Procedure Combilyzer_process_result(test, res: ansistring; var value, units, tag:ansistring);
var t, v, u:ansistring;
    i:integer;
begin

   case  IndexStr (test, ['UBG', 'BIL', 'KET', 'CRE', 'BLD', 'PRO', 'ALB', 'NIT', 'LEU', 'GLU', 'SG', 'pH', 'VC', 'A:C', 'RT']) of
      0:
      begin
           for i:=2 to 7 do t:=t+res[i];
           t:=Trim(t);
           if t='Normal' then
            begin
                v:=trim(res[8]+res[9]+res[10]);
                t:='';  //zelba na Sanije da ne piselo Normal
            end
           else
           begin
               t:=t[1]+t[2];
               v:=trim(res[6]+res[7]+res[8]+res[9]+res[10]);
           end;
           u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
      end;
      1,2,9:
      begin
           for i:=2 to 4 do t:=t+res[i];
           t:=trim(t);
           if t='Neg' then
           begin
                u:='';
                v:='';
           end
           else
           begin
                v:=trim(res[6]+res[7]+res[8]+res[9]+res[10]);
                u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
           end;

      end;
      4,8:
      begin
           for i:=2 to 4 do t:=t+res[i];
           t:=trim(t);
           if t='Neg' then
           begin
                u:='';
                v:='';
           end
           else
           begin
                t:=t[1]+t[2];
                v:=trim(res[4]+res[5]+res[6]+res[7]+res[8]+res[9]+res[10]);
                u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
                if test ='BLD' then v:=StringReplace(v, 'Ca', ' Er',[]);
                if test ='LEU' then v:=StringReplace(v, 'Ca', ' Le',[]);
           end;

      end;
      3,6,12:
      begin
           t:='';
           v:=trim(res[7]+res[8]+res[9]+res[10]);
           u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
      end;
      5:
      begin
           for i:=2 to 6 do t:=t+res[i];
           t:=trim(t);
           if t='Neg' then
           begin
                u:='';
                v:='';
           end
           else
            if t='Trace' then v:='(T����)'
            else
            begin
                t:=t[1]+t[2];
                v:=trim(res[4]+res[5]+res[6]+res[7]+res[8]+res[9]+res[10]);
                u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
            end;

      end;
      7:
      begin
           t:='';
           v:=trim(res[2]+res[3]+res[4]+res[5]);
           u:='';
      end;
      10,11:
      begin
           t:='';
           v:=trim(res[2]+res[3]+res[4]+res[5]+res[6]+res[7]+res[8]+res[9]+res[10]);
           u:='';
      end;
      14:
      begin
           t:='';
           v:=trim(res);
           u:='';
      end;
      13:
      begin
           t:='';
           v:=trim(res[2]+res[3]+res[4]+res[5]+res[6]+res[7]+res[8]+res[9]+res[10]);
          // if v='<3.4' then v:=v+' Normal';
         //  if v='3.4-33.9' then v:=v+' Abnormal';
         //  if v='>=33.9' then v:=v+' High abn.';
           u:=trim(res[11]+res[12]+res[13]+res[14]+res[15]+res[16]+res[17]);
      end;
   end;
   tag:=t;
   value:=v;
   units:=u;
end;


end.
