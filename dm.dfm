object dm1: Tdm1
  OldCreateOrder = False
  Height = 526
  Width = 841
  object dsresbuffer: TDataSource
    AutoEdit = False
    DataSet = resultsbuffer
    Left = 40
    Top = 76
  end
  object resultsbuffer: TVirtualTable
    IndexFieldNames = 'test'
    MasterSource = dsPatBuffer
    MasterFields = 'sampleID'
    DetailFields = 'sampleID'
    FieldDefs = <
      item
        Name = 'sampleID'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'result'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'test'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'deviceid'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'source_result'
        DataType = ftString
        Size = 50
      end>
    AfterDelete = resultsbufferAfterDelete
    Left = 40
    Top = 20
    Data = {
      04000500080073616D706C65494401000F00000000000600726573756C740100
      14000000000004007465737401000A0000000000080064657669636569640100
      0600000000000D00736F757263655F726573756C740100320000000000000000
      000000}
    object resultsbuffersampleID: TStringField
      DisplayLabel = 'ID '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082
      FieldName = 'sampleID'
      Size = 15
    end
    object resultsbufferresult: TStringField
      DisplayLabel = #1056#1077#1079#1091#1083#1090#1072#1090
      FieldName = 'result'
    end
    object resultsbuffertest: TStringField
      DisplayLabel = #1040#1085#1072#1083#1080#1079#1072
      FieldName = 'test'
    end
    object resultsbufferdeviceid: TStringField
      FieldName = 'deviceid'
      Size = 6
    end
    object resultsbuffersource_result: TStringField
      DisplayLabel = #1080#1079#1074#1086#1088#1077#1085' '#1088#1077#1079'.'
      FieldName = 'source_result'
      Size = 50
    end
    object resultsbufferunit: TStringField
      DisplayLabel = #1052#1077#1088'. '#1077#1076'.'
      FieldName = 'unit'
      Size = 10
    end
  end
  object qpatients: TOraQuery
    Session = OraSession
    SQL.Strings = (
      
        'select o.brdnevn, o.god1, o.reid,  o.barkod, os.imepr pacient, o' +
        's.embg, rodenden(os.embg) datum_rod, decode(os.pol, 1, '#39#1052#1072#1096#1082#1080#39','#39 +
        #1046#1077#1085#1089#1082#1080#39') pol, o.primen from oddelenie o, osigurenici os , ambula' +
        'nti a '
      
        'where o.embg=os.embg and o.brdnevn=a.brdnevn and o.reid=a.reid a' +
        'nd o.god1=a.god1'
      'and o.barkod=:sampleID')
    MasterSource = dsPatBuffer
    MasterFields = 'sampleID'
    DetailFields = 'BARKOD'
    Options.CacheLobs = False
    Left = 184
    Top = 28
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SAMPLEID'
        ParamType = ptInput
        Value = nil
      end>
    object qpatientsBRDNEVN: TIntegerField
      DisplayWidth = 12
      FieldName = 'BRDNEVN'
      Required = True
    end
    object qpatientsGOD1: TIntegerField
      DisplayWidth = 12
      FieldName = 'GOD1'
      Required = True
    end
    object qpatientsBARKOD: TStringField
      DisplayWidth = 16
      FieldName = 'BARKOD'
      Size = 13
    end
    object qpatientsEMBG: TFloatField
      DisplayWidth = 12
      FieldName = 'EMBG'
    end
    object qpatientsDATUM_ROD: TDateTimeField
      DisplayWidth = 22
      FieldName = 'DATUM_ROD'
    end
    object qpatientsPACIENT: TStringField
      DisplayWidth = 31
      FieldName = 'PACIENT'
      Size = 4000
    end
    object qpatientsREID: TIntegerField
      DisplayWidth = 12
      FieldName = 'REID'
      Required = True
    end
    object qpatientsPOL: TStringField
      DisplayWidth = 4
      FieldName = 'POL'
      Size = 1
    end
    object qpatientsPRIMEN: TDateTimeField
      FieldName = 'PRIMEN'
      Required = True
    end
  end
  object dsPatients: TOraDataSource
    DataSet = qpatients
    Left = 400
    Top = 108
  end
  object dsResults: TOraDataSource
    AutoEdit = False
    DataSet = qresults
    Left = 400
    Top = 44
  end
  object qresults: TOraQuery
    SQLUpdate.Strings = (
      'UPDATE REZULTATI'
      'SET'
      '  REZULTAT = :REZULTAT'
      'WHERE'
      '  ID = :Old_ID')
    Session = OraSession
    SQL.Strings = (
      
        'select i.brdnevn, i.reid, i.god1, r.id, r.rezultat, vr.nazrezult' +
        ', vr.podreduvanje, r.ref_min, r.ref_max, r.merna_ed, r.ref_vredn' +
        'ost, la.aliasname '
      'from rezultati r, ispituv i, vidrezult vr, lis_aliases la '
      'where '
      'i.brdnevn=:BRDNEVN and i.reid=:REID and i.god1=:GOD1 and '
      
        'r.rezultid=vr.rezultid and r.datum=i.datum and r.brispit=i.brisp' +
        'it and vr.rezultid=la.testid'
      
        '-- and la.instrumentid=(select v1 from sys_setap where p1='#39'combi' +
        'lyzer'#39') --and r.isready=0'
      
        ' and la.instrumentid in (select id from lis_instruments where no' +
        'te='#39'cxlis'#39')'
      'order by podreduvanje')
    MasterSource = dsPatients
    MasterFields = 'BRDNEVN;REID;GOD1'
    DetailFields = 'BRDNEVN;REID;GOD1'
    FetchRows = 100
    FetchAll = True
    Left = 240
    Top = 28
    ParamData = <
      item
        DataType = ftInteger
        Name = 'BRDNEVN'
        ParamType = ptInput
        Value = nil
      end
      item
        DataType = ftInteger
        Name = 'REID'
        ParamType = ptInput
        Value = nil
      end
      item
        DataType = ftInteger
        Name = 'GOD1'
        ParamType = ptInput
        Value = nil
      end>
    object qresultsBRDNEVN: TIntegerField
      FieldName = 'BRDNEVN'
    end
    object qresultsREID: TIntegerField
      FieldName = 'REID'
    end
    object qresultsGOD1: TIntegerField
      FieldName = 'GOD1'
    end
    object qresultsID: TFloatField
      FieldName = 'ID'
    end
    object qresultsREZULTAT: TStringField
      DisplayLabel = #1056#1077#1079#1091#1083#1090#1072#1090
      DisplayWidth = 50
      FieldName = 'REZULTAT'
      Size = 200
    end
    object qresultsNAZREZULT: TStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1072#1084#1077#1090#1072#1088
      FieldName = 'NAZREZULT'
      Size = 50
    end
    object qresultsPODREDUVANJE: TFloatField
      FieldName = 'PODREDUVANJE'
    end
    object qresultsREF_MIN: TFloatField
      FieldName = 'REF_MIN'
    end
    object qresultsREF_MAX: TFloatField
      FieldName = 'REF_MAX'
    end
    object qresultsMERNA_ED: TStringField
      FieldName = 'MERNA_ED'
    end
    object qresultsREF_VREDNOST: TStringField
      DisplayLabel = #1056#1077#1092#1077#1088#1077#1085#1090#1085#1072' '#1074#1088#1077#1076#1085#1086#1089#1090
      DisplayWidth = 100
      FieldName = 'REF_VREDNOST'
      Size = 300
    end
    object qresultsALIASNAME: TStringField
      DisplayLabel = #1040#1085#1072#1083#1080#1079#1072
      FieldName = 'ALIASNAME'
      Size = 50
    end
  end
  object OraSession: TOraSession
    Options.Charset = 'WE8MSWIN1252'
    Options.Direct = True
    LoginPrompt = False
    Left = 120
    Top = 28
  end
  object PatientsBuffer: TVirtualTable
    Left = 40
    Top = 148
    Data = {04000000000000000000}
    object PatientsBuffersampleID: TStringField
      DisplayLabel = 'ID '#1085#1072' '#1087#1088#1080#1084#1077#1088#1086#1082
      FieldName = 'sampleID'
      Size = 15
    end
  end
  object dsPatBuffer: TDataSource
    AutoEdit = False
    DataSet = PatientsBuffer
    OnDataChange = dsPatBufferDataChange
    Left = 40
    Top = 204
  end
  object OraErrorHandler1: TOraErrorHandler
    Session = OraSession
    OnError = OraErrorHandler1Error
    Left = 112
    Top = 88
  end
  object FBconn: TFDConnection
    Params.Strings = (
      'Protocol=TCPIP'
      'Port=3050'
      'CharacterSet=WIN1251'
      'User_Name=sysdba'
      'Password=Root'
      'DriverID=FB')
    ConnectedStoredUsage = [auDesignTime]
    LoginPrompt = False
    Transaction = FDTransaction1
    OnError = FBconnError
    Left = 576
    Top = 88
  end
  object fqpatients: TFDQuery
    MasterSource = dsPatBuffer
    MasterFields = 'sampleID'
    DetailFields = 'BARKOD'
    Connection = FBconn
    Transaction = FDTransaction1
    SQL.Strings = (
      
        'select a.broj brdnevn, a.godina god1, a.re reid ,b.embg , b.datu' +
        'm_ragjanje datum_rod, b.ime ||'#39' '#39'|| b.prezime pacient,'
      
        '(case when b.pol=0 then '#39#1046#1077#1085#1089#1082#1080#39' else '#39#1052#1072#1096#1082#1080#39' end) pol, a.barkod' +
        ', a.datum primen'
      'from pzz_pregledi a, pzz_osigurenici b'
      'where a.karton_broj=b.karton_broj and  a.barkod=:sampleID')
    Left = 576
    Top = 168
    ParamData = <
      item
        Name = 'SAMPLEID'
        DataType = ftString
        ParamType = ptInput
        Value = ''
      end>
    object fqpatientsBRDNEVN: TIntegerField
      FieldName = 'BRDNEVN'
      Origin = 'BROJ'
      Required = True
    end
    object fqpatientsGOD1: TSmallintField
      FieldName = 'GOD1'
      Origin = 'GODINA'
      Required = True
    end
    object fqpatientsREID: TIntegerField
      FieldName = 'REID'
      Origin = 'RE'
      Required = True
    end
    object fqpatientsEMBG: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'EMBG'
      Origin = 'EMBG'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqpatientsDATUM_ROD: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'DATUM_ROD'
      Origin = 'DATUM_RAGJANJE'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqpatientsPACIENT: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PACIENT'
      Origin = 'PACIENT'
      ProviderFlags = []
      ReadOnly = True
      Size = 151
    end
    object fqpatientsPOL: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'POL'
      Origin = 'POL'
      ProviderFlags = []
      ReadOnly = True
      FixedChar = True
      Size = 6
    end
    object fqpatientsBARKOD: TStringField
      FieldName = 'BARKOD'
      Origin = 'BARKOD'
      Size = 13
    end
    object fqpatientsPRIMEN: TDateField
      FieldName = 'PRIMEN'
      Origin = 'DATUM'
    end
  end
  object fqresults: TFDQuery
    MasterSource = dsPatients
    MasterFields = 'BRDNEVN;REID;GOD1'
    DetailFields = 'BRDNEVN;REID;GOD1'
    Connection = FBconn
    Transaction = FDTransaction1
    UpdateOptions.AssignedValues = [uvEInsert, uvAutoCommitUpdates]
    UpdateOptions.EnableInsert = False
    UpdateOptions.AutoCommitUpdates = True
    UpdateObject = fUpdateqresults
    SQL.Strings = (
      
        'select r.broj brdnevn, r.re reid,  r.godina god1 , r.redbr id, r' +
        '.vrednost rezultat,'
      
        ' vr.naziv  nazrezult, vr.podreduvanje, la.minval ref_min, la.max' +
        'val ref_max, la.mes_units merna_ed, vr.ref_vrednosti ref_vrednos' +
        't, la.aliasname'
      
        'from pzz_analizi r,  pzz_def_analizi vr, lis_aliases la --, lis_' +
        'instruments li'
      'where '
      'r.broj=:BRDNEVN and r.re=:REID and r.godina=:GOD1 and'
      'r.analiza=vr.id and vr.parent=la.testid   --vr.id=la.testid '
      
        '-- and la.instrumentid=li.id and (li.note='#39'urina'#39' or li.note='#39'a1' +
        '5'#39')'
      
        'and la.instrumentid in (select id from lis_instruments where not' +
        'e = '#39'cxlis'#39')'
      'order by podreduvanje')
    Left = 640
    Top = 168
    ParamData = <
      item
        Name = 'BRDNEVN'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'REID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'GOD1'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end>
    object fqresultsBRDNEVN: TIntegerField
      FieldName = 'BRDNEVN'
      Origin = 'BROJ'
    end
    object fqresultsREID: TIntegerField
      FieldName = 'REID'
      Origin = 'RE'
      Required = True
    end
    object fqresultsGOD1: TSmallintField
      FieldName = 'GOD1'
      Origin = 'GODINA'
      Required = True
    end
    object fqresultsID: TIntegerField
      FieldName = 'ID'
      Origin = 'REDBR'
      Required = True
    end
    object fqresultsREZULTAT: TStringField
      DisplayLabel = #1056#1077#1079#1091#1083#1090#1072#1090
      DisplayWidth = 50
      FieldName = 'REZULTAT'
      Origin = 'VREDNOST'
      Size = 100
    end
    object fqresultsNAZREZULT: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1072#1088#1072#1084#1077#1090#1072#1088
      FieldName = 'NAZREZULT'
      Origin = 'NAZIV'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object fqresultsPODREDUVANJE: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'PODREDUVANJE'
      Origin = 'PODREDUVANJE'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqresultsREF_MIN: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'REF_MIN'
      Origin = 'MINVAL'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqresultsREF_MAX: TSingleField
      AutoGenerateValue = arDefault
      FieldName = 'REF_MAX'
      Origin = 'MAXVAL'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqresultsMERNA_ED: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'MERNA_ED'
      Origin = 'MES_UNITS'
      ProviderFlags = []
      ReadOnly = True
    end
    object fqresultsREF_VREDNOST: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = #1056#1077#1092#1077#1088#1077#1085#1090#1085#1072' '#1074#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'REF_VREDNOST'
      Origin = 'REF_VREDNOSTI'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object fqresultsALIASNAME: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = #1040#1085#1072#1083#1080#1079#1072
      FieldName = 'ALIASNAME'
      Origin = 'ALIASNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
  end
  object FDTransaction1: TFDTransaction
    Connection = FBconn
    Left = 640
    Top = 88
  end
  object fAssays: TFDQuery
    DetailFields = 'BRDNEVN;REID;GOD1'
    Connection = FBconn
    Transaction = FDTransaction1
    SQL.Strings = (
      '')
    Left = 640
    Top = 232
  end
  object qAssays: TOraQuery
    SQLUpdate.Strings = (
      '')
    Session = OraSession
    FetchRows = 100
    FetchAll = True
    Left = 240
    Top = 156
  end
  object qQuerytext: TOraQuery
    Session = OraSession
    SQL.Strings = (
      'select * from lis_querytext where methodname like '#39'cxlis%'#39)
    Left = 176
    Top = 160
    object qQuerytextID: TFloatField
      FieldName = 'ID'
      Required = True
    end
    object qQuerytextMETHODNAME: TStringField
      FieldName = 'METHODNAME'
      Size = 100
    end
    object qQuerytextSQLTEXT: TStringField
      FieldName = 'SQLTEXT'
      Size = 2048
    end
  end
  object fQuerytext: TFDQuery
    Connection = FBconn
    SQL.Strings = (
      'select * from lis_querytext where methodname like '#39'cxlis%'#39)
    Left = 576
    Top = 232
    object fQuerytextID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object fQuerytextMETHODNAME: TStringField
      FieldName = 'METHODNAME'
      Origin = 'METHODNAME'
      Size = 100
    end
    object fQuerytextSQLTEXT: TStringField
      FieldName = 'SQLTEXT'
      Origin = 'SQLTEXT'
      Size = 2048
    end
  end
  object dsQuerytext: TDataSource
    AutoEdit = False
    DataSet = qQuerytext
    Left = 400
    Top = 168
  end
  object dsAssays: TDataSource
    AutoEdit = False
    DataSet = qAssays
    Left = 400
    Top = 232
  end
  object fAnalyzers: TFDQuery
    Connection = FBconn
    SQL.Strings = (
      
        'select a.*, b.analyzer_name, b.protocol from lis_ws_ports_config' +
        ' a, lis_protocols b where a.analyzer_protocol =b.id')
    Left = 576
    Top = 288
  end
  object qAnalyzers: TOraQuery
    Session = OraSession
    SQL.Strings = (
      
        'select a.*, b.analyzer_name, b.protocol from lis_ws_ports_config' +
        ' a, lis_protocols b where a.analyzer_protocol =b.id')
    Left = 168
    Top = 216
  end
  object dsAnalyzers: TDataSource
    AutoEdit = False
    DataSet = qAnalyzers
    Left = 400
    Top = 288
  end
  object qPatientList: TOraQuery
    Session = OraSession
    Left = 240
    Top = 216
  end
  object fPatientList: TFDQuery
    Connection = FBconn
    Transaction = FDTransaction1
    Left = 640
    Top = 288
  end
  object dsPatientList: TDataSource
    AutoEdit = False
    DataSet = qPatientList
    Left = 400
    Top = 344
  end
  object AssaysToLoad: TVirtualTable
    Left = 40
    Top = 296
    Data = {04000000000000000000}
    object AssaysToLoadid: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'id'
      KeyFields = 'id'
    end
    object AssaysToLoadbarcode: TStringField
      FieldName = 'barcode'
    end
    object AssaysToLoadassay: TStringField
      FieldName = 'assay'
    end
    object AssaysToLoadpatientID: TStringField
      FieldName = 'patientID'
    end
    object AssaysToLoadpatientName: TStringField
      FieldName = 'patientName'
      Size = 60
    end
    object AssaysToLoadsex: TStringField
      FieldName = 'sex'
    end
    object AssaysToLoaddob: TDateField
      FieldName = 'dob'
    end
    object AssaysToLoadmessage: TStringField
      FieldName = 'message'
      Size = 300
    end
  end
  object fGetPatient: TFDQuery
    Connection = FBconn
    Transaction = FDTransaction1
    Left = 576
    Top = 344
  end
  object qGetPatient: TOraQuery
    Session = OraSession
    Left = 240
    Top = 272
  end
  object dsGetPatient: TDataSource
    AutoEdit = False
    DataSet = qGetPatient
    Left = 400
    Top = 392
  end
  object mAnalyzers: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 40
    Top = 360
    object mAnalyzerscomm_reference: TStringField
      FieldName = 'comm_reference'
    end
    object mAnalyzersanalyzer_id: TStringField
      FieldName = 'analyzer_id'
      Size = 10
    end
    object mAnalyzersanalyzer_protocol: TStringField
      FieldName = 'analyzer_protocol'
    end
    object mAnalyzerscomport: TStringField
      FieldName = 'comport'
      Size = 5
    end
    object mAnalyzersbaud: TStringField
      FieldName = 'baud'
      Size = 10
    end
    object mAnalyzersdata: TStringField
      FieldName = 'data'
      Size = 5
    end
    object mAnalyzersparity: TStringField
      FieldName = 'parity'
      Size = 5
    end
    object mAnalyzersstop: TStringField
      FieldName = 'stop'
      Size = 5
    end
    object mAnalyzershandflow: TStringField
      FieldName = 'handflow'
      Size = 10
    end
    object mAnalyzerscommunication_type: TStringField
      FieldName = 'communication_type'
    end
    object mAnalyzersfile_type: TStringField
      FieldName = 'file_type'
      Size = 5
    end
    object mAnalyzersfile_path: TStringField
      FieldName = 'file_path'
      Size = 200
    end
    object mAnalyzersfile_load_path: TStringField
      FieldName = 'file_load_path'
      Size = 200
    end
    object mAnalyzersmanual_load: TStringField
      FieldName = 'manual_load'
      KeyFields = 'analyzer_id'
      Size = 5
    end
  end
  object fUpdateqresults: TFDUpdateSQL
    Connection = FBconn
    Left = 720
    Top = 208
  end
  object qAliases: TOraQuery
    Session = OraSession
    SQL.Strings = (
      'select * from LIS_ALIASES')
    Left = 240
    Top = 88
  end
  object fAliases: TFDQuery
    Connection = FBconn
    SQL.Strings = (
      'select * from LIS_ALIASES')
    Left = 720
    Top = 168
  end
  object dsAliases: TDataSource
    AutoEdit = False
    DataSet = qAliases
    Left = 400
    Top = 8
  end
end
