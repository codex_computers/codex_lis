unit UnitHC100;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils,Vcl.Dialogs, Vcl.Controls ;
 procedure hc100_proceed(filename:string);
 procedure hc100loadassays(panalyzerid:string; pbarcodes:tstrings);
 Procedure hc100CreateHeader(var precord:string);
 Procedure HC100CreatePatient(num:integer; pbarcode:string; var precord:string);
 Procedure hc100CreateComment(num:integer; var precord:string);
 Procedure HC100CreateAssay(num:integer; passay:string; var precord:string);
 Procedure hc100CreateEnd(var precord:string);
 Procedure hc100CreateRecord(p_fields:Tstrings; p_sep:char; var p_record:string);
const
  pAnalyzer:string='HumaStar 100'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm, UnitCommon;

procedure hc100loadassays(panalyzerid:string; pbarcodes:tstrings);
var i,j:integer;
    t: Textfile;
    line: string;
    load_path, filename:string;
    day, month, year, hour, min:string;
    assays_list:tstrings;
    display_msg:string;
begin
     assays_list:=tstringlist.Create;
      month:=inttostr(Monthof(now));
      if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
     day:=inttostr(Dayof(now));
     if strtoint(day)<10 then day:='0'+day;
     hour:=inttostr(Hourof(now));
     if strtoint(hour)<10 then hour:='0'+hour;
     min:=inttostr(Minuteof(now));
     if strtoint(min)<10 then min:='0'+min;
     load_path:=UnitCommon.GetAnalyzerProperty('file', 'load_file_path');
     filename:=load_path+'HC100'+inttostr(Yearof(now))+month+day+hour+min+'.astm';
     Assignfile(t, filename);
     Rewrite(t);
     try
        hc100createheader(line);
        WriteLn(t, line);
        for i:=0 to pbarcodes.Count-1  do
        begin
          dm1.GetPatientOpen(pbarcodes[i]);
          hc100CreatePatient(i+1, pbarcodes[i], line);
          WriteLn(t, line);
       //   hc100CreateComment(i+1,line);
       //   WriteLn(t, line);
          dm1.getassays(pbarcodes[i], panalyzerid, assays_list);
          display_msg:='Send to: ANALYZER='+pAnalyzer+'     BARCODE='+pbarcodes[i]+'     ASSAYS=[';
          for j:=0 to assays_list.Count-1 do
          begin
               hc100CreateAssay(j+1, assays_list[j], line);
               WriteLn(t, line);
               display_msg:=display_msg+assays_list[j]+', ';
          end;
          display_msg:=copy(display_msg,1,length(display_msg)-2)+']';
          FrmMain.SetTextColor('blue');
          frmMain.Memo1.lines.add(display_msg);
          FrmMain.SetTextColor('black');
        end;
       hc100CreateEnd(line);
       WriteLn(t, line);
     finally
        Closefile(t);
     end;
end;


procedure hc100_proceed(filename:string);
var
  t: Textfile;
  line: string;
  elements: TStringlist;
  i:integer;
  pbarcode:string;
  display_msg:string;
begin
  Assignfile(t, filename);
  Reset(t);
  try
    elements := TStringlist.Create;
    try
      while not Eof(t) do
      begin
        ReadLn(t, line);
        {The following ignores empty lines}
        if dm1.IScan('|', line, 1) > 0 then
        begin
          elements.clear;
          dm1.SplitString(line, '|', elements);
          //zapis vo mem tabeli za HS100
          if trim(elements[0])='P' then pbarcode:=elements[3];  //zemam barkod
          if trim(elements[0])='R' then  //obrabotuvam arezultati za barkod
          begin
            if (dm1.PatientsBuffer.Locate('sampleID',pbarcode,[])) then
            begin
               dm1.PatientsBuffer.edit;
               dm1.PatientsBuffersampleID.Value:=pbarcode;
               dm1.PatientsBuffer.Post;
            end
            else
            begin
            //  FrmMain.memo1.Lines.Add('Sample ID: '+pbarcode);
              dm1.PatientsBuffer.Append;
              dm1.PatientsBuffersampleID.Value:=pbarcode;
              dm1.PatientsBuffer.Post;
            end;
            dm1.resultsbuffer.Append;
            dm1.resultsbufferSampleid.Value:=pbarcode;
            dm1.resultsbufferresult.Value:=elements[8];
            dm1.resultsbuffertest.value:=elements[2];
            dm1.resultsbufferunit.value:=elements[4];
            display_msg:='     ANALYZER='+panalyzer+'    SAMPLEID='+pbarcode+'    TEST='+elements[2]+'   '+'RESULT='+elements[8]+'    UNIT='+elements[4];
           { for i := 0 to elements.count-1 do
            begin

                if i=0 then dm1.resultsbufferSampleid.Value:=pbarcode;
                if i=3 then dm1.resultsbufferresult.Value:=elements[i];
                if i=1 then dm1.resultsbuffertest.value:=elements[i];
                if i=4 then dm1.resultsbufferunit.value:=elements[i];

            end;  }
            dm1.resultsbuffer.Post;
            FrmMain.memo1.Lines.Add(display_msg);
          end;
        end;
      end;
    finally
      elements.Free
    end;
  finally
    Closefile(t);
    if messagedlg('����� '+filename+' � ������� ������. ���� ������ �� �� �������� ���� � ���������?'
    , mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes  then
    begin
         renamefile(filename, extractFilepath(filename)+'procesed\procesed_'+extractFilename(filename));
         deletefile(filename);
    end;
  end;

end;

Procedure hc100CreateHeader(var precord:string);
var i:integer;
    pfields:tstrings;
    month, day, rec:string;
begin
       precord:='H|\^&|||HS100^V1.0|||||cxLis||P|1|';
       month:=inttostr(Monthof(now));
       if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
       day:=inttostr(Dayof(now));
       if strtoint(day)<10 then day:='0'+day;
       precord:=precord+inttostr(Yearof(now))+month+day;
end;
Procedure HC100CreatePatient(num:integer; pbarcode:string; var precord:string);
var
    pfields:tstrings;
    day, month, year:string;
    i:integer;
begin
       dm1.GetPatientOpen(pbarcode);
       pfields:=tstringlist.Create;
       for i:=1 to 34 do pfields.add('');
       pfields[0]:='P';
       pfields[1]:=inttostr(num);
       pfields[3]:=pbarcode;
       month:=inttostr(Monthof(dm1.dsGetPatient.DataSet.FieldByName('DOB').AsDateTime));
       if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
       day:=inttostr(Dayof(dm1.dsGetPatient.DataSet.FieldByName('DOB').AsDateTime));
       if strtoint(day)<10 then day:='0'+day;
       year:=inttostr(Yearof(dm1.dsGetPatient.DataSet.FieldByName('DOB').AsDateTime));
       pfields[7]:=year+month+day;
       hc100CreateRecord(pfields,'|',precord);
end;

Procedure HC100CreateAssay(num:integer; passay:string; var precord:string);
var
    pfields:tstrings;
    i:integer;
begin
       pfields:=tstringlist.Create;
       for i:=1 to 31  do
           pfields.Add('');
       pfields[0]:='O';
       pfields[1]:=inttostr(num);
       pfields[4]:=passay;
       pfields[5]:='False';
       pfields[15]:='Serum';
       hc100CreateRecord(pfields,'|',precord);
end;

Procedure hc100CreateEnd(var precord:string);
begin
       precord:='L||N';
end;
Procedure hc100CreateComment(num:integer; var precord:string);
begin
       precord:='C|'+inttostr(num)+'|||';
end;

Procedure hc100CreateRecord(p_fields:Tstrings; p_sep:char; var p_record:string); //generiranje na ASTM rekordi
var i:integer;
month, day:string;
begin
     p_record:='';
     for i:=0 to p_fields.Count-2 do
     p_record:=p_record+p_fields[i]+p_sep;
     p_record:=p_record+p_fields[p_fields.Count-1];
end;
end.
