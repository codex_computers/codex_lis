unit UnitAnalyzerLoad;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.Actions, Vcl.ActnList, Vcl.ComCtrls, Vcl.Buttons, Data.DB, MemDS,
  VirtualTable;

type

  TfrmAnalyzerLoad = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    StatusBar1: TStatusBar;
    BitBtn4: TBitBtn;
    ActionList1: TActionList;
    aExit: TAction;
    aClear: TAction;
    aLoad: TAction;
    aPatLoad: TAction;
    Memo1: TMemo;
    BitBtn2: TBitBtn;
    RadioGroup1: TRadioGroup;
    tAnalyzers: TVirtualTable;
    tAnalyzersid: TIntegerField;
    tAnalyzersprotocol: TStringField;
    tAnalyzerscomm: TStringField;
    tAnalyzersanalyzerid: TIntegerField;
    procedure aExitExecute(Sender: TObject);
    procedure aPatLoadExecute(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
       analyzer_id:string;
    { Public declarations }
  end;

var
  frmAnalyzerLoad: TfrmAnalyzerLoad;
  dict_key:string;
  barcode_list:tstrings; //za da vratam lista od barkodovi pa da ja predadam na Vidas3
implementation
 uses UnitSelPatients, dm, UnitCommon, UnitVidas3, UnitHC100;
{$R *.dfm}

procedure TfrmAnalyzerLoad.aExitExecute(Sender: TObject);
begin
     frmAnalyzerLoad.Close;
end;

procedure TfrmAnalyzerLoad.aPatLoadExecute(Sender: TObject);
begin
     frmSelPAtients:=TfrmSelPatients.Create(frmAnalyzerLoad);
     tAnalyzers.Locate('id', radiogroup1.ItemIndex,[]);
     dm1.PatListOpen(tAnalyzersanalyzerid.AsString);
     frmSelPatients.showmodal;
     if frmSelPatients.ModalResult=MrOk then
     begin
          memo1.Lines:=frmSelPatients.barcode_list;
     end;
     frmSelPAtients.Free;
     StatusBar1.SimpleText:='�������� �����:'+inttostr(memo1.Lines.Count);
end;

procedure TfrmAnalyzerLoad.BitBtn2Click(Sender: TObject);

begin
     if memo1.Lines.Count>0 then
     begin
          if tAnalyzersProtocol.AsString='vidas3' then
          begin
               UnitVidas3.vidas3loadassays(tAnalyzersComm.AsString, memo1.Lines);
          end;
          if tAnalyzersProtocol.AsString='humastar100' then
          begin
               UnitHC100.hc100loadassays(tAnalyzersanalyzerid.AsString, memo1.Lines);  //tuka davm id na analajzer za da povlecam assays vo proc
          end;

     end;
     frmAnalyzerLoad.Close;
end;

procedure TfrmAnalyzerLoad.BitBtn4Click(Sender: TObject);
begin
     memo1.Clear;
     StatusBar1.SimpleText:='�������� �����:'+inttostr(memo1.Lines.Count);
end;

procedure TfrmAnalyzerLoad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     tAnalyzers.Close;
end;

procedure TfrmAnalyzerLoad.FormShow(Sender: TObject);
begin
     tAnalyzers.Open;
     dm1.mAnalyzers.First;
     while not dm1.mAnalyzers.Eof do
     begin
          if dm1.mAnalyzersmanual_load.AsString='1' then
          begin
              tAnalyzers.insert;
              tAnalyzerscomm.asstring:=dm1.mAnalyzerscomm_reference.AsString;
              tAnalyzersid.asinteger:=RadioGroup1.Items.Add(dm1.mAnalyzersanalyzer_protocol.AsString);
              tAnalyzersProtocol.asString:=dm1.mAnalyzersanalyzer_protocol.AsString;
              tAnalyzersAnalyzerID.AsInteger:=strtoint(dm1.mAnalyzersanalyzer_id.AsString);
              tAnalyzers.Post;
          end;
          dm1.mAnalyzers.Next;
     end;

end;

end.
