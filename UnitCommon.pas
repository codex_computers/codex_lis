unit UnitCommon;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils,Vcl.Dialogs, System.Variants, System.Classes,
VaComm, dateUtils, System.Generics.Collections, FireDAC.Comp.DataSet;

Procedure FillAnalyzerDicionary;
function is_number(palias, panalyzer_id:string):integer;
function convert_to_number(presult:string; pdecplaces:integer):string;
function GetAnalyzerProperty(pdict_key:string; pproperty:string):string;
function Dec2Bin(iDec: Integer): string;
function BinToHex(BinStr: string): string;

implementation
 uses dm;



 Procedure FillAnalyzerDicionary;   //cita analajzeri i definicija za niv
 var i:integer;
     comm_reference:string;
 begin
       dm1.dsAnalyzers.DataSet.Close;
       dm1.dsAnalyzers.DataSet.Filter:='WS_ID='+dm1.ws_id;
       dm1.dsAnalyzers.DataSet.Open;
       dm1.dsAnalyzers.dataset.Filtered:=true;
       dm1.mAnalyzers.Open;


       i:=1;
       while not dm1.dsAnalyzers.dataset.eof do
       begin
            dm1.mAnalyzers.Insert;
            dm1.mAnalyzersanalyzer_id.asstring:=dm1.dsAnalyzers.dataset.FieldByName('ANALYZER_ID').AsString;
            dm1.mAnalyzersanalyzer_protocol.asstring:=dm1.dsAnalyzers.dataset.FieldByName('PROTOCOL').AsString;
            dm1.mAnalyzerscomport.asstring:=dm1.dsAnalyzers.dataset.FieldByName('COM_PORT').AsString;
            dm1.mAnalyzersbaud.asstring:=dm1.dsAnalyzers.dataset.FieldByName('BAUD').AsString;
            dm1.mAnalyzersdata.asstring:=dm1.dsAnalyzers.dataset.FieldByName('DATA').AsString;
            dm1.mAnalyzersparity.asstring:=dm1.dsAnalyzers.dataset.FieldByName('PARITY').AsString;
            dm1.mAnalyzersstop.asstring:=dm1.dsAnalyzers.dataset.FieldByName('STOP').AsString;
            dm1.mAnalyzershandflow.asstring:=dm1.dsAnalyzers.dataset.FieldByName('HANDFLOW').AsString;
            dm1.mAnalyzerscommunication_type.asstring:=dm1.dsAnalyzers.dataset.FieldByName('CONNECTION_TYPE').AsString;
            if dm1.dsAnalyzers.dataset.FieldByName('CONNECTION_TYPE').AsString='file' then
            begin
                 dm1.mAnalyzersfile_type.asstring:='true';
                 dm1.mAnalyzersfile_path.asstring:=dm1.dsAnalyzers.dataset.FieldByName('FILE_PATH').AsString;
                 dm1.mAnalyzersfile_load_path.asstring:=dm1.dsAnalyzers.dataset.FieldByName('LOAD_FILE_PATH').AsString;
            end
            else  dm1.mAnalyzersfile_type.asstring:='false';
            if (dm1.mAnalyzersanalyzer_protocol.asstring='vidas3') or (dm1.mAnalyzersanalyzer_protocol.asstring='humastar100') then dm1.mAnalyzersmanual_load.asstring:='1'  //privremeno resenie , treba da go definiram vo bazata
            else dm1.mAnalyzersmanual_load.asstring:='0';

            if dm1.mAnalyzerscommunication_type.asstring='com' then dm1.mAnalyzerscomm_reference.asstring:='VaComm'+inttostr(i);
            if dm1.mAnalyzerscommunication_type.asstring='file' then dm1.mAnalyzerscomm_reference.asstring:='file';
            if dm1.mAnalyzerscommunication_type.asstring='tcp' then dm1.mAnalyzerscomm_reference.asstring:='tcp';   //ovoa za idni aparati na tcp port
            dm1.mAnalyzers.Post;

            inc(i);
            dm1.dsAnalyzers.dataset.Next;

       end;


 end;

 function is_number(palias, panalyzer_id:string):integer;   //proverka dali aliasot e definiran kako numericki rezultat
 begin
      if dm1.dsAliases.DataSet.locate('ALIASNAME;INSTRUMENTID',VarArrayOf([palias, panalyzer_id]), []) then
      begin

           if dm1.dsAliases.DataSet.FieldByName('ISDIGITAL').asinteger=1 then
             result:=dm1.dsAliases.DataSet.FieldByName('DECDIGITS').asinteger
           else result:=-1;
      end
      else
      result:=-2;
 end;

 function convert_to_number(presult:string; pdecplaces:integer):string;   //zaokruzuvanje na numericki rezultati spored definicija
 var pnum_res:extended;
     pres_temp, formatstring:string;
     i:integer;
 begin
      formatstring:='#,##0';
      if pdecplaces>0 then
      begin
           formatstring:=formatstring+'.';
           for I := 1 to pdecplaces do formatstring:=formatstring+'0';
      end;

      pnum_res:=strtofloat(presult);

      result:=FormatFloat(formatstring,pnum_res);
 end;

 function GetAnalyzerProperty(pdict_key:string; pproperty:string):string;  //za prebaruvanje po konekcija i vrakjanje na vrednost po property od aparat
 var value:string;

 begin
      if dm1.mAnalyzers.Locate('comm_reference', pdict_key, []) then
      begin
         if pproperty='protocol' then  value:=dm1.mAnalyzersanalyzer_protocol.asstring;
         if pproperty='id' then  value:=dm1.mAnalyzersanalyzer_id.asstring;
         if pproperty='file_path' then  value:=dm1.mAnalyzersfile_path.asstring;
         if pproperty='manual_load' then  value:=dm1.mAnalyzersmanual_load.asstring;
         if pproperty='load_file_path' then  value:=dm1.mAnalyzersfile_load_path.asstring;
         if pproperty='file_path' then  value:=dm1.mAnalyzersfile_path.asstring;
         result:=value;
      end;


 end;

 function Dec2Bin(iDec: Integer): string;
begin
  Result:='';
while iDec>0 do
  begin
    Result:=IntToStr(iDec and 1)+Result;
    iDec:=iDec shr 1;
  end;
end;

 function BinToHex(BinStr: string): string;
const
  BinArray: array[0..15, 0..1] of string =
    (('0000', '0'), ('0001', '1'), ('0010', '2'), ('0011', '3'),
     ('0100', '4'), ('0101', '5'), ('0110', '6'), ('0111', '7'),
     ('1000', '8'), ('1001', '9'), ('1010', 'A'), ('1011', 'B'),
     ('1100', 'C'), ('1101', 'D'), ('1110', 'E'), ('1111', 'F'));
var
  Error: Boolean;
  j: Integer;
  BinPart: string;
begin
  Result:='';

  Error:=False;
  for j:=1 to Length(BinStr) do
    if not (BinStr[j] in ['0', '1']) then
    begin
      Error:=True;
     // ShowMessage('This is not binary number');
      Break;
    end;

  if not Error then
  begin
    case Length(BinStr) mod 4 of
      1: BinStr:='000'+BinStr;
      2: BinStr:='00'+BinStr;
      3: BinStr:='0'+BinStr;
    end;

    while Length(BinStr)>0 do
    begin
      BinPart:=Copy(BinStr, Length(BinStr)-3, 4);
      Delete(BinStr, Length(BinStr)-3, 4);
      for j:=1 to 16 do
        if BinPart=BinArray[j-1, 0] then
          Result:=BinArray[j-1, 1]+Result;
    end;
  end;
end;
end.
