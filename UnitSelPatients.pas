unit UnitSelPatients;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ComCtrls, Vcl.StdCtrls, System.Actions, Vcl.ActnList;

type
  TfrmSelPatients = class(TForm)
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    aSelectAll: TAction;
    aExit: TAction;
    aGetSelected: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure aSelectAllExecute(Sender: TObject);
    procedure aExitExecute(Sender: TObject);
    procedure aGetSelectedExecute(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    barcode_list:tstrings;
  end;

var
  frmSelPatients: TfrmSelPatients;

implementation
 uses dm;
{$R *.dfm}

procedure TfrmSelPatients.aExitExecute(Sender: TObject);
begin
     frmSelPatients.Close;
     frmSelPatients.ModalResult:=mrCancel;
end;

procedure TfrmSelPatients.aGetSelectedExecute(Sender: TObject);
var
      i: Integer;
      s: string;

begin
      barcode_list:=tstringlist.create;
      if DBGrid1.SelectedRows.Count>0 then
        with DBGrid1.DataSource.DataSet do
          for i:=0 to DBGrid1.SelectedRows.Count-1 do
          begin
            GotoBookmark(DBGrid1.SelectedRows.Items[i]);
            s:=fields.Fields[4].AsString;
            barcode_list.Add(s);
            s:= '';
          end;


     frmSelPatients.Close;
     frmSelPatients.ModalResult:=mrOk;
end;

procedure TfrmSelPatients.aSelectAllExecute(Sender: TObject);
begin
     with dbGrid1.datasource.dataset do
     begin
        disableControls;
        first;
        while not(eof) do
           begin
            dbGrid1.SelectedRows.CurrentRowSelected:=true;
            next;
           end;
        enableControls;
     end;
end;

procedure TfrmSelPatients.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key = VK_RETURN then  aGetSelectedExecute(Sender);

end;

procedure TfrmSelPatients.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dm1.dsPatientList.DataSet.Close;
end;

procedure TfrmSelPatients.FormShow(Sender: TObject);
begin
   dbgrid1.Columns.Items[0].Width:=300;
end;

end.
