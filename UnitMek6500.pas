unit UnitMek6500;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;

 procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
 Procedure mek6500_parse(data:ansistring);
var
  barcode:string;
const
  pAnalyzer:string='Celltac MEK6500'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm;

procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
var
  I, j: Integer;
  Fmessage: string;
begin
    j:=1; //reden broj na redica vo porakata, taka se odreduvat parametrite i ostanatite podatoci
    for I := 1 to Length(data) do
     case data[I] of
      chr($0D):  //Waiting for this?
        begin
          mek6500_parse(inttostr(j)+'|'+FMessage);
          inc(j);
         // memo2.Lines.Add(fmessage);
          FMessage := '';  //reset received message
        end;
      else
        FMessage := FMessage + data[I];
     end;

end;


Procedure mek6500_parse(data:ansistring);
 var rownum, position, i,j:integer;
    row, sample_id, alias, datum, result,mark, qunit:string;
begin

      position:=pos('|', data);
      for i := 1 to position-1 do row:=row+data[i];
      rownum:=strtoint(row);
      if rownum=1 then   //prv red e datumot
      begin
            for i:=position+1 to length(data) do datum := datum+data[i];
      end;
      if rownum=2 then   //vtor red e sample id
      begin
           for i:=position+1 to length(data) do sample_id:=sample_id+data[i];
           sample_id:=trim(sample_id);
           if not (dm1.PatientsBuffer.Locate('sampleID',sample_id,[])) then
           begin
                dm1.PatientsBuffer.Append;   //zapis na barkod dokolku ne postoi vo buffer dataset
                dm1.PatientsBuffersampleID.Value:=sample_id;
                dm1.PatientsBuffer.Post;
                barcode:=sample_id;   //koristime globalna promenliva kako kaj kombilyzer zaradi nacinot na koj prefrla podatoci
           end;

      end;
      if (rownum>2) and (rownum<=21)  then  //obrabotka na rezultati, dadeni se po red imora da se mapirat so aliasi
      begin
            case rownum of
            3: alias:='WBC';
            4: alias:='LY%';
            5: alias:='MO%';
            6: alias:='GR%';
            7: alias:='LY';
            8: alias:='MO';
            9: alias:='GR';
            10: alias:='RBC';
            11: alias:='HGB';
            12: alias:='HCT';
            13: alias:='MCV';
            14: alias:='MCH';
            15: alias:='MCHC';
            16: alias:='RDW-CV';
            17: alias:='PLT';
            18: alias:='PCT';
            19: alias:='MPV';
            20: alias:='PDW';
            end;
            if (alias<>'') then
            begin
                 for i:=position+1 to position+4 do result:=result+data[i];
                 for i:=position+5 to position+6  do mark:=mark+data[i];  //dokolku ima abnormalni vrednosti ili drugi nepravilnosti
                 FrmMain.memo1.Lines.Add('     ANALYZER='+pAnalyzer+'    SAMPLEID='+barcode+'    TEST='+alias+'   '+'RESULT='+result+'    MARK='+mark+'    UNIT='+qunit);
                 dm1.resultsbuffer.Append;
                 dm1.resultsbufferSampleid.Value:=barcode;
                 if result<>'' then dm1.resultsbufferresult.Value:=trim(result);
                 dm1.resultsbuffertest.value:=alias;
                 dm1.resultsbufferunit.value:=qunit;
                 dm1.resultsbuffer.Post;
            end;

      end;



end;
end.
