unit UnitMaglumi;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;
    procedure RxChar(Sender: TObject; Count: Integer; comm: tVaComm);
    procedure MaglumiEndRecord(pfields: tstrings; pcomm:tVacomm);
    procedure MaglumiPatientRecord(pfields: tstrings);
    procedure MaglumiRequestQuery(pfields: tstrings);
    procedure MaglumiResultrecord(pfields: tstrings);
    procedure MaglumiHeaderRecord(pfields: tstrings);
    procedure MaglumiAnswerQuery(pstatus,pbarcode:string; pserial:integer; pcomm: tVaComm);
    procedure MaglumiAssayRecord(pfields: tstrings);
    Procedure maglumi_parse(data:ansistring; pcomm:tVacomm);
    function IScan(ch: Char; const S: string; fromPos: Integer): Integer;
    procedure SplitString(const S: string; separator: Char; substrings: TStrings);
    Procedure MaglumiCreateRecord(p_fields:Tstrings; var p_record:string);
    Procedure MaglumiCreateHeader(var precord:string);
    Procedure MaglumiCreatePatient(var precord:string);
    Procedure MaglumiCreateAssay(passay:string; pserial:integer; var precord:string);
    Procedure MaglumiCreateEnd(var precord:string);
var
    state:string;    //status na app dali prima rezultati ili prakja aliasi
    state_send:string;  //status za prakjanje na kontrolni karaktri pri odgovor
    barcode:string;
    serialno:integer;
    Fmessage:string;
const
    pAnalyzer:string='Maglumi2000'; //promenliva za treansfer za koj aparat/protokol se rabote
implementation
uses unit3, dm, unitCommon;

procedure RxChar(Sender: TObject; Count: Integer; comm:tVaComm);
var data:string;
i:integer;
begin
    // pAnalyzer:=p_analyzer;
     data:=comm.ReadText;
     if FrmMain.mode='debug' then FrmMain.memo2.Lines.add(data);
     for I := 1 to Length(data) do
     case data[I] of
      #2: comm.WriteText(#6);
      #3: comm.WriteText(#6);
      #5: comm.WriteText(#6);
      #4:begin
             comm.WriteText(#6);
             if state='query' then  MaglumiAnswerQuery('',barcode, serialno,comm); //inicijalizacija za response
         end;
      #6:if (state='query') and (state_send<>'') then MaglumiAnswerQuery(state_send,barcode, serialno,comm);

      chr($0D):  //CR za obrabotka na porakata
        begin

          maglumi_parse(FMessage, comm);
         // memo2.Lines.Add(fmessage);
          FMessage := '';  //reset received message
        end;
      else
        FMessage := FMessage + data[I];
     end;
end;

function IScan(ch: Char; const S: string; fromPos: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := fromPos to Length(S) do
  begin
    if S[i] = ch then
    begin
      Result := i;
      Break;
    end;
  end;
end;
  //nema da trebat  slednite dve proc
procedure SplitString(const S: string; separator: Char; substrings: TStrings);
var
  i, n: Integer;
begin
  if Assigned(substrings) and (Length(S) > 0) then
  begin
    i := 1;
    repeat
      n := IScan(separator, S, i);
      if n = 0 then
        n := Length(S) + 1;
      substrings.Add(Copy(S, i, n - i));
      i := n + 1;
    until
      i > Length(S);
  end;
end;


Procedure maglumi_parse(data:ansistring; pcomm:tVacomm);
var fields:tstrings;
i:integer;
begin
  fields:=TStringlist.Create;
  dm1.splitstring(data, '|', fields);

    case  IndexStr (fields[0], ['H','Q', 'R','P','L','O']) of
    0: MaglumiHeaderRecord(fields);
    1: MaglumiRequestQuery(fields);
    2: MaglumiResultrecord(fields);
    3: MaglumiPatientRecord(fields);
    4: MaglumiEndRecord(fields, pcomm);
    5: MaglumiAssayRecord(fields);
    end;

end;

Procedure MaglumiHeaderRecord(pfields:tstrings);
begin
    // frmmain.Memo1.Lines.Add('ANALYZER:'+pfields[4]);
end;
Procedure MaglumiRequestQuery(pfields:tstrings);
begin
     FrmMain.SetTextColor('red');
     frmMain.Memo1.Lines.Add('ANALYZER='+panalyzer+'    Request for sampleID:'+copy(pfields[2],2, length(pfields[2]))+'    Ser.no='+pfields[1]);
     FrmMain.SetTextColor('black');
     serialno:=strtoint(pfields[1]);
     barcode:=copy(pfields[2],2,length(pfields[2]));

     state:='query';   //rezim na baranje analizi za barkod
end;
Procedure MaglumiResultrecord(pfields:tstrings);
var  pserial,passay, presult, punit, pflag, display_msg:string;
begin

     if state='result' then
     begin
          pserial:=pfields[1];
          passay:=copy(pfields[2],4, length(pfields[2]));
          presult:=pfields[3];
          punit:=pfields[4];
          pflag:=pfields[6];
          display_msg:='     ANALYZER='+panalyzer+'    SAMPLEID='+barcode+'    TEST='+passay+'   '+'RESULT='+presult+'    FLAG='+pflag+'    UNIT='+punit;
          FrmMain.Memo1.Lines.Add(display_msg);

         if (presult<>'') then
          begin
               dm1.resultsbuffer.Append;
               dm1.resultsbufferSampleid.Value:=barcode;
               dm1.resultsbufferresult.Value:=trim(presult);
               dm1.resultsbuffertest.value:=passay;
               dm1.resultsbufferunit.value:=punit;
               dm1.resultsbuffer.Post;
          end;
     end;
end;
Procedure MaglumiPatientRecord(pfields:tstrings);
begin
      serialno:=strtoint(pfields[1]);
      state:='result';
end;
Procedure MaglumiEndRecord(pfields:tstrings; pcomm:tVacomm);
begin
     if (pfields[2]='N')  then
        pcomm.WriteText(#6);

end;
Procedure MaglumiAssayRecord(pfields:tstrings);
var  pserial, passay:string;
begin
     if state='result' then
     begin
        pserial:=pfields[1];
        barcode:=pfields[2];
        if strtoint(pserial)=serialno then //proverka dali se rabote za istiot serial t.e. za istata poraka
        begin
           if not (dm1.PatientsBuffer.Locate('sampleID',barcode,[])) then
           begin
                dm1.PatientsBuffer.Append;   //zapis na barkod dokolku ne postoi vo buffer dataset
                dm1.PatientsBuffersampleID.Value:=barcode;
                dm1.PatientsBuffer.Post;
           end;
        end;
     end;
end;
Procedure MaglumiAnswerQuery(pstatus, pbarcode:string; pserial:integer; pcomm: tVaComm); //podgoovk za prakjanje na aliasi po barkod
var i, j:integer;
    messagerecord:String;
    display_msg:string;
    assays_list:tstrings;
begin
     case IndexStr (pstatus, ['','enq', 'stx', 'cr','etx','eot']) of
     0:begin
              pcomm.WriteText(#5);
              state_send:='enq';
      end;
     1:begin
              pcomm.WriteText(#2);
              state_send:='stx';
       end;
     2:begin    //tuka trebe da se kreirat rekordite
              assays_list:=tstringlist.Create;
              MaglumiCreateHeader(messagerecord);
              pcomm.WriteText(messagerecord+chr($0D));
              MaglumiCreatePatient(messagerecord);
              pcomm.WriteText(messagerecord+chr($0D));
              display_msg:='ANALYZER='+panalyzer+'    SAMPLEID='+barcode+'    ASSAYS=[';



              dm1.getassays(pbarcode, UnitCommon.GetAnalyzerProperty(pcomm.Name, 'id') , assays_list);  //povlekuvam aliasi za barkod po aparat

              for j:=0 to assays_list.Count-1 do    //vrtam za site aliasi sho se naznaceni za pacientot
              begin
                   MaglumiCreateAssay(assays_list[j], j+1, messagerecord);
                   display_msg:=display_msg+assays_list[j]+', '; //se kreira stringot za prikaz vo memo
                   pcomm.WriteText(messagerecord+chr($0D));
              end;
              MaglumiCreateEnd(messagerecord);
              pcomm.WriteText(messagerecord+chr($0D));
              display_msg:=copy(display_msg,1,length(display_msg)-2)+']';    //ispis koi analizi se prateni kon analajzer
              FrmMain.SetTextColor('blue');
              frmMain.Memo1.Lines.Add(display_msg);
              FrmMain.SetTextColor('black');

              state_send:='cr';
       end;
     3:begin
              pcomm.WriteText(#3);
              state_send:='etx';
          end;
     4:begin
              pcomm.WriteText(#4);
              state_send:='eot';
          end;
     5:begin
              state:='';
              state_send:='';     //clear states
          end;

     end;




end;

Procedure MaglumiCreateRecord(p_fields:Tstrings; var p_record:string); //generiranje na ASTM rekordi
var i:integer;
month, day:string;
begin
     p_record:='';
     for i:=0 to p_fields.Count-2 do
     p_record:=p_record+p_fields[i]+'|';
     p_record:=p_record+p_fields[p_fields.Count-1];
end;

Procedure MaglumiCreateHeader(var precord:string);
var i:integer;
    pfields:tstrings;
    month, day, rec:string;
begin
       pfields:=tstringlist.Create;
       pfields.add('H');
       pfields.add('\^&');
       pfields.add('');
       pfields.add('PSWD');
       pfields.add('Lis');
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('');
       pfields.add('Maglumi2000');
       pfields.add('');
       pfields.add('P');
       pfields.add('E1394-97');
       month:=inttostr(Monthof(now));
       if strtoint(month)<10 then month:='0'+month;  //popolnuvanje so 0 za mesec<10
       day:=inttostr(Dayof(now));
       if strtoint(day)<10 then day:='0'+day;
       pfields.add(inttostr(Yearof(now))+month+day);
       MaglumiCreateRecord(pfields, precord);
end;

Procedure MaglumiCreatePatient(var precord:string);
var i:integer;
    pfields:tstrings;
begin
       pfields:=tstringlist.Create;
       pfields.add('P');
       pfields.add(inttostr(serialno));
       MaglumiCreateRecord(pfields,precord);
end;

Procedure MaglumiCreateAssay(passay:string; pserial:integer; var precord:string);
// pserial e zaradi razlikata vo protokol na 600 i 2000, vo primerot za 600
// pseial za sekoj nov assay se zgolemuva, a ja probav so site isti kako serial od
// patient record i mi minese. po default neka e serial od patient record
var i:integer;
    pfields:tstrings;
begin
       pfields:=tstringlist.Create;
       pfields.add('O');
       pfields.add(inttostr(serialno));
     //  pfields.add(inttostr(i));
       pfields.add(barcode);
       pfields.add('');
       pfields.add('^^^'+passay);
       pfields.add('R');
       MaglumiCreateRecord(pfields,precord);
end;

Procedure MaglumiCreateEnd(var precord:string);
var i:integer;
    pfields:tstrings;
begin
       pfields:=tstringlist.Create;
       pfields.add('L');
       pfields.add(inttostr(serialno));
       pfields.add('N');
       MaglumiCreateRecord(pfields,precord);
end;

end.
