unit UnitVidasPC;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils,Vcl.Dialogs, Vcl.Controls;
procedure vidaspc_proceed(filename:string);
const
  pAnalyzer:string='VidasPC'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm;

procedure vidaspc_proceed(filename:string);
var
  t: Textfile;
  line: string;
  elements: TStringlist;
  i:integer;
begin
  Assignfile(t, filename);
  Reset(t);
  try
    elements := TStringlist.Create;
    try
      while not Eof(t) do
      begin
        ReadLn(t, line);
        {The following ignores empty lines}
        if (dm1.IScan(#44, line, 1) > 0) and (line[2]<>'V') then
        begin
          elements.clear;
          dm1.SplitString(line, #44, elements);
          //zapis vo mem tabeli za a15
          if (dm1.PatientsBuffer.Locate('sampleID',copy(elements[4],2,length(elements[4])-2),[])) then
          begin
               dm1.PatientsBuffer.edit;
               dm1.PatientsBuffersampleID.Value:=copy(elements[4],2,length(elements[4])-2); //copy se koriste za da se otstranat navodnicite
               dm1.PatientsBuffer.Post;
          end
          else
          begin
              dm1.PatientsBuffer.Append;
              dm1.PatientsBuffersampleID.Value:=copy(elements[4],2,length(elements[4])-2);
              dm1.PatientsBuffer.Post;
          end;
          dm1.resultsbuffer.Append;
          for i := 0 to elements.count-1 do
          begin

                if i=4 then dm1.resultsbufferSampleid.Value:=copy(elements[i],2,length(elements[i])-2);
                if i=6 then dm1.resultsbufferresult.Value:=copy(elements[i],2,length(elements[i])-2);
                if i=5 then dm1.resultsbuffertest.value:=copy(elements[i],2,length(elements[i])-2);
                if i=7 then dm1.resultsbufferunit.value:=copy(elements[i],2,length(elements[i])-2);

          end;
          dm1.resultsbuffer.Post;
          FrmMain.memo1.Lines.Add('   ANALYZER='+panalyzer+'    Sample ID='+dm1.resultsbufferSampleid.asstring+'    TEST='+dm1.resultsbuffertest.asstring+'   '+'RESULT='+dm1.resultsbufferresult.asstring+'   UNIT='+dm1.resultsbufferunit.asstring);
        end;
      end;
    finally
      elements.Free
    end;
  finally
    Closefile(t);
    if messagedlg('����� '+filename+' � ������� ������. ���� ������ �� �� �������� ���� � ���������?'
    , mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrYes  then
     renamefile(filename, extractFilepath(filename)+'procesed\procesed_'+extractFilename(filename));
  end;

end;
end.
