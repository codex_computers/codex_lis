unit UnitAbl9;

interface
uses System.StrUtils, Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
VaComm, dateUtils;

 procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
 Procedure abl9_parse(data:ansistring);
 Procedure Abl9ResultRecord(pfields:tstrings);
 Procedure Abl9PatientRecord(pfields:tstrings);
var
  barcode:string;
  serialno:integer;

const
  pAnalyzer:string='Radiometer ABL9'; //globalna promenliva za naziv na analyzer/protokol
implementation

uses unit3,dm;

procedure VaCapture1Message(Sender: TObject; const Data: AnsiString);
var
  I: Integer;
  Fmessage: string;
begin
    for I := 1 to Length(data) do
     case data[I] of
      chr($0D):  //Waiting for this?
        begin
          abl9_parse(FMessage);

         // memo2.Lines.Add(fmessage);
          FMessage := '';  //reset received message
        end;
      else
        FMessage := FMessage + data[I];
     end;

end;


Procedure abl9_parse(data:ansistring);
var fields:tstrings;
i:integer;
begin
  fields:=TStringlist.Create;
  dm1.splitstring(data, '|', fields);

    case  IndexStr (fields[0], ['H','R','P','L']) of
    0:;  // Abl9HeaderRecord(fields);
    1: Abl9Resultrecord(fields);
    2: Abl9PatientRecord(fields);
    3:;// Abl9EndRecord(fields);
    end;

end;

Procedure Abl9ResultRecord(pfields:tstrings);
var  pserial,passay, presult,presult_type, punit, pflag, display_msg:string;
     assayname:tstrings;
begin
          assayname:=tstringlist.Create;
          dm1.splitstring(pfields[2],'^',assayname);

          passay:=assayname[3];
          presult_type:=assayname[4];

          presult:=pfields[3];
          punit:=pfields[4];
          pflag:=pfields[6];
          display_msg:='     ANALYZER='+panalyzer+'    SAMPLEID='+barcode+'    TEST='+passay+'   '+'RESULT='+presult+'    FLAG='+pflag+'    UNIT='+punit;
          FrmMain.Memo1.Lines.Add(display_msg);

         if (presult<>'') and (presult<>'.....') and (presult<>'?.....') then     // gi isklucuvam parametrite sho ne vrakjat rezultat i imat nekoj komentar
          begin
               dm1.resultsbuffer.Append;
               dm1.resultsbufferSampleid.Value:=barcode;
               dm1.resultsbufferresult.Value:=trim(presult);
               dm1.resultsbuffertest.value:=passay;
               dm1.resultsbufferunit.value:=punit;
               dm1.resultsbuffer.Post;
          end;

end;
Procedure Abl9PatientRecord(pfields:tstrings);
begin
        barcode:=pfields[3];
        if not (dm1.PatientsBuffer.Locate('sampleID',barcode,[])) then
        begin
             dm1.PatientsBuffer.Append;   //zapis na barkod dokolku ne postoi vo buffer dataset
             dm1.PatientsBuffersampleID.Value:=barcode;
             dm1.PatientsBuffer.Post;
        end;

end;
end.
