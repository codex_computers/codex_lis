object frmSelPatients: TfrmSelPatients
  Left = 0
  Top = 0
  ActiveControl = DBGrid1
  Caption = #1048#1079#1073#1086#1088' '#1085#1072' '#1087#1072#1094#1080#1077#1085#1090#1080
  ClientHeight = 375
  ClientWidth = 658
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 658
    Height = 353
    Align = alTop
    Color = clSkyBlue
    DataSource = dm1.dsPatientList
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Calibri'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnKeyDown = DBGrid1KeyDown
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 356
    Width = 658
    Height = 19
    Panels = <
      item
        Text = 
          'F3 - '#1057#1077#1083#1077#1082#1090#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077'   Enter - '#1054#1073#1088#1072#1073#1086#1090#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080'   Esc - '#1048 +
          #1079#1083#1077#1079
        Width = 300
      end>
  end
  object ActionList1: TActionList
    Left = 568
    Top = 120
    object aSelectAll: TAction
      Caption = 'aSelectAll'
      ShortCut = 114
      OnExecute = aSelectAllExecute
    end
    object aExit: TAction
      Caption = 'aExit'
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aExitExecute
    end
    object aGetSelected: TAction
      Caption = 'aGetSelected'
      ShortCut = 120
      OnExecute = aGetSelectedExecute
    end
  end
end
